<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/auth', 'Auth\LoginController@index')->name('login');

Route::get('/car/guest', 'CarPageController@index');
Route::get('/car/guest/vue', 'CarPageController@indexVue');
Route::get('/car/guest/{id}', 'CarPageController@show');
Route::get('/car/guest/{id}/get', 'CarPageController@showVue');
Route::get('/cars/guest/{carId}/get/rating/{ownerId}', 'CarPageController@getRating');

Route::get('/tour/guest', 'TourPageController@index');
Route::get('/tour/guest/vue', 'TourPageController@indexVue');
Route::get('/tour/guest/{id}', 'TourPageController@show');
Route::get('/tour/guest/{id}/get', 'TourPageController@showVue');
Route::get('/tour/guest/{tourId}/get/rating/{ownerId}', 'TourPageController@getRating');

Route::get('/hotels', 'HotelPageController@index');
Route::get('/hotels/vue', 'HotelPageController@indexVue');
Route::get('/hotels/{id}', 'HotelPageController@show');
Route::get('/hotels/{id}/get', 'HotelPageController@showVue');
Route::get('/hotels/show/room/{id}', 'HotelPageController@showRooms');
Route::get('/hotels/show/room/{id}/get', 'HotelPageController@showRoomsVue');
Route::get('/hotels/{roomId}/get/rating/{ownerId}', 'HotelPageController@getRating');

Route::get('/house/guest', 'HomePageController@index');
Route::get('/house/guest/vue', 'HomePageController@indexVue');
Route::get('/house/guest/{id}', 'HomePageController@show');
Route::get('/house/guest/{id}/get', 'HomePageController@showVue');
Route::get('/house/guest/{roomId}/get/rating/{ownerId}', 'HomePageController@getRating');

Route::get('/flights', 'FlightController@index');
Route::get('/cars', 'CarController@index');
Route::get('/tours', 'TourController@index');
Route::get('/404', function () {
    return view('error.404');
});

/**
 * Booking
 * ---------------------
 */

Route::get('/book/{id}/{type}', 'BookingController@index');
Route::get('/book/{id}/all/data/{type}', 'BookingController@recoupData');
Route::post('/book/{id}/{type}', 'BookingController@store');

/**
 * Booking complete -- Thank you
 * ------------------------------------
 */

Route::get('/thank-you', 'ThanksController@index');

/**
 * This is notes after a user logs in
 * POST-LOGIN
 */

Route::post('/auth/login', 'Auth\LoginController@store')->name('auth-login');
Route::post('/auth/logout', 'Auth\LoginController@destroy');
Route::post('/auth/sign-up/customer', 'Auth\RegisterController@storeCustomer');
Route::post('/auth/sign-up/hotel', 'Auth\RegisterController@storeHotel');
Route::post('/auth/sign-up/home', 'Auth\RegisterController@storeHome');
Route::post('/auth/sign-up/car', 'Auth\RegisterController@storeCar');
Route::post('/auth/sign-up/tour', 'Auth\RegisterController@storeTour');

Route::get('/wizard', 'WizardController@index')->name('wizard-view');
Route::post('/wizard', 'WizardController@store');

Route::get('/dashboard', 'DashboardController@index');

Route::get('/admin/amenity', 'AmenityController@index');
Route::get('/admin/amenity/all', 'AmenityController@getAmenities');
Route::post('/admin/amenity', 'AmenityController@store');
Route::patch('/admin/amenity/edit/{id}', 'AmenityController@update');
Route::delete('/admin/amenity/destroy/{id}', 'AmenityController@destroy');

Route::get('/admin/agreement', 'AdminAgreementController@index');
Route::get('/admin/agreement/all', 'AdminAgreementController@getAgreements');
Route::patch('/admin/agreement', 'AdminAgreementController@update');

Route::get('/admin/roomtype', 'RoomTypeController@index');
Route::get('/admin/roomtype/all', 'RoomTypeController@getRoomtype');
Route::post('/admin/roomtype', 'RoomTypeController@store');
Route::patch('/admin/roomtype/edit/{id}', 'RoomTypeController@update');
Route::delete('/admin/roomtype/destroy/{id}', 'RoomTypeController@destroy');

Route::get('/admin/users', 'UsersController@index');
Route::get('/admin/users/{usertype}', 'UsersController@show');
Route::get('/admin/users/get/{usertype}', 'UsersController@getUsers');

Route::get('/admin/booking', 'AdminBookingController@index');
Route::get('/admin/booking/{usertype}', 'AdminBookingController@show');
Route::get('/admin/booking/owners/{usertype}', 'AdminBookingController@getOwners');

/**
 * Rooms - Hotels
 * ----------------------
 */

Route::get('/hotel/rooms', 'RoomController@index');
Route::post('/hotel/rooms', 'RoomController@store');
Route::get('/hotel/rooms/edit/{id}', 'RoomController@updateIndex');
Route::patch('/hotel/rooms/edit/{id}', 'RoomController@update');
Route::delete('/hotel/rooms/destroy/{id}', 'RoomController@destroy');
Route::patch('/hotel/rooms/edit/image/{id}', 'RoomController@updateImage');
Route::delete('/hotel/rooms/image/destroy/{id}', 'RoomController@imageDestruction');
Route::get('/hotel/rooms/all', 'RoomController@getData');
Route::get('/hotel/rooms/real/{id}', 'RoomController@getRealData');
Route::get('/hotel/rooms/create', 'RoomController@create');
Route::get('/hotel/rooms/image/{id}', 'RoomController@showImage');
Route::get('/hotel/rooms/images/all/{id}', 'RoomController@getImages');

Route::get('/hotel/booking', 'HotelBookingController@index');
Route::get('/hotel/booking/all', 'HotelBookingController@getBookedUsers');

Route::get('/hotel/agreement', 'HotelAgreementController@index');
Route::get('/hotel/agreement/get', 'HotelAgreementController@retrieveData');
Route::patch('/hotel/agreement/update/{id}', 'HotelAgreementController@update');

/**
 * Home -- Properties
 * ----------------------
 */

Route::get('/home/property', 'PropertyController@index');
Route::post('/home/property', 'PropertyController@store');
Route::get('/home/property/edit/{id}', 'PropertyController@updateIndex');
Route::patch('/home/property/edit/{id}', 'PropertyController@update');
Route::patch('/home/property/destroy/{id}', 'PropertyController@destroy');
Route::patch('/home/property/edit/image/{id}', 'PropertyController@updateImage');
Route::patch('/home/property/image/destroy/{id}', 'PropertyController@imageDestruction');
Route::get('/home/property/all', 'PropertyController@getData');
Route::get('/home/property/real/{id}', 'PropertyController@getRealData');
Route::get('/home/property/image/{id}', 'PropertyController@showImage');
Route::get('/home/property/images/all/{id}', 'PropertyController@getImages');
Route::get('/home/property/create', 'PropertyController@create');

Route::get('/home/booking', 'HomeBookingController@index');
Route::get('/home/booking/all', 'HomeBookingController@getBookedUsers');

Route::get('/home/agreement', 'HomeAgreementController@index');
Route::get('/home/agreement/get', 'HomeAgreementController@retrieveData');
Route::patch('/home/agreement/update/{id}', 'HomeAgreementController@update');

/**
 * Car
 * ----------------------
 */
Route::get('/car/cars', 'CarController@getCar');
Route::post('/car/cars', 'CarController@store');
Route::patch('/car/cars/edit/{id}', 'CarController@update');
Route::patch('/car/cars/edit/image/{id}', 'CarController@updateImage');
Route::delete('/car/cars/image/{id}', 'CarController@imageDestruction');
Route::get('/car/cars/all', 'CarController@getData');
Route::get('/car/cars/image', 'CarController@showImage');
Route::get('/car/cars/image/all', 'CarController@getImages');
Route::get('/car/cars/create', 'CarController@create');
Route::get('/car/booking', 'CarBookingController@index');
Route::get('/car/booking/all', 'CarBookingController@getCarUser');
Route::get('/car/agreement', 'CarAgreementController@index');


Route::get('/car/driver', 'DriverController@index');
Route::get('/car/drivers/all', 'DriverController@getDrivers');
Route::post('/drivers/create', 'DriverController@store');
Route::delete('/drivers/destroy/{id}', 'DriverController@destroy');

/**
 * Tour
 * ----------------------
 */

Route::get('/tour/tours', 'TourController@getTour');
Route::post('/tour/tours', 'TourController@store');
Route::get('/tour/tours/edit/{id}', 'TourController@updateIndex');
Route::patch('/tour/tours/edit/{id}', 'CarController@update');
Route::patch('/tour/tours/edit/image/{id}', 'TourController@updateImage');
Route::delete('/tour/tours/image/{id}', 'TourController@imageDestruction');
Route::get('/tour/tours/all', 'TourController@getData');
Route::get('/tour/tours/real/{id}', 'TourController@getRealData');
Route::get('/tour/tours/image', 'TourController@showImage');
Route::get('/tour/tours/image/all', 'TourController@getImages');
Route::get('tour/tours/create', 'TourController@create');
Route::get('/tour/booking', 'TourBookingController@index');
Route::get('/tour/booking/all', 'TourBookingController@getTourUsers');
Route::get('/tour/agreement', 'TourAgreementController@index');

/**
 * Agreement
 * -------------------
 */

Route::post('/agree', 'AgreementController@agree');
Route::get('/agree/check/{agreement_id}', 'AgreementController@checkAgreement');

/**
 * Review
 * ----------------
 */

Route::post('/review/{type}/store', 'ReviewController@store');
Route::post('/review/{type}/update', 'ReviewController@update');
Route::post('/review/{type}/destroy', 'ReviewController@destroy');