<?php

namespace Tests\Unit;

use App\Car;
use App\CarBooking;
use App\CarCompanyOwner;
use App\CarImage;
use App\Driver;
use App\Review;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CarTest extends TestCase
{
    use DatabaseMigrations;
    private $user, $carOwner, $carImage, $driverFactory, $driver, $bookCar, $car;
    private $review;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->car = new Car();
        $this->carOwner = new CarCompanyOwner();
        $this->carImage = new CarImage();
        $this->driver = new Driver();
        $this->review = new Review();
        $this->bookCar = new CarBooking();
        $this->driverFactory = factory(Driver::class)->make();
    }
    /**
     * car_has_owner
     * @test
     * @return void
     */
    public function car_has_owner()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $created_car_owner = $this->carOwner->createCarOwner($createdOwner->id, 'description', 'logo', 'company', 48, 24, 48, 24);

        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.00, 'available');

        $this->assertEquals(
            $createdCar->carOwner->company_name,
            $created_car_owner->company_name
        );
    }

    /**
     * car_has_images
     * @test
     * @return void
     */
    public function car_has_images()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.00, 'available');

        $createdImage = $this->carImage->addSingleImage(
            $createdCar->id,
            ['signedUrl' => 'https://cloudinary.com/more/profile/kuku/bird.png']
        );
        $this->assertEquals(
            $createdCar->images[0]->image,
            $createdImage->image
        );
    }

    /**
     * driver_has_name
     * @test
     * @return void
     */
    public function driver_has_name()
    {
        $this->assertNotEmpty($this->driverFactory->name);
    }

    /**
     * get_driver_by_car
     * @test Relationship
     * @return void
     */
    public function get_driver_by_car()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdDriver = $this->driver->createDriver('name', 'description');
        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, $createdDriver->id, 200.00, 'available');

        $this->assertEquals(
            $createdCar->driver->name,
            $createdDriver->name
        );
    }

    /**
     * get_reviews_from_car
     * @test Relationship
     * @return void
     */
    public function get_reviews_from_car()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdDriver = $this->driver->createDriver('name', 'description');
        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, $createdDriver->id, 200.00, 'available');
        $createdReview = $this->review->createReview($createdCustomer->id, $createdOwner->id, 'this is the review', 5, null, $createdCar->id, null, null);

        $this->assertEquals(
            $createdCar->reviews[0]->review,
            $createdReview->review
        );
    }

    /**
     * get_bookings_from_car
     * @test Relationship
     * @return void
     */
    public function get_bookings_from_car()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $this->carOwner->createCarOwner($createdOwner->id, 'description', 'logo', 'company', 48, 24, 48, 24);

        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.00, 'available');
        $registerCarBooking = $this->bookCar->bookCar($createdCustomer->id, $createdCar->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 'extra', '222222222222222222');
        $this->assertEquals(
            $createdCar->bookings[0]->charge_id,
            $registerCarBooking->charge_id
        );
    }
}