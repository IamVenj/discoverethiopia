<?php

namespace Tests\Unit;

use App\Agreement;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\User;
use App\UserAgreement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    private $user, $review, $agreement, $agreementUser, $userFactory;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->agreement = new Agreement();
        $this->agreementUser = new UserAgreement();
        $this->userFactory = factory(User::class)->make();
    }

    /**
     * get_agreed_agreement_by_user
     * @test relationship
     * @return void
     */
    public function get_agreed_agreement_by_user()
    {
        $createUser = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 3);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);

        $agreementCreate = $this->agreement->createAgreement(
            $createdOwner->id,
            'this is the term and condition'
        );
        $addedAgreement = $this->agreementUser->addAgreement(
            $createUser->id,
            $agreementCreate->id
        );
        $this->assertEquals(
            $addedAgreement->agreements[0]->terms_and_conditions,
            $agreementCreate->terms_and_conditions
        );
    }

    /**
     * user_is_assigned_role_when_creating_customer
     * @test
     * @return void
     */
    public function user_is_assigned_role_when_creating_user()
    {
        $this->assertNotEmpty($this->userFactory->role);
    }

    /**
     * login_with_credentials
     * @test
     * @return void
     */
    public function login_with_credentials()
    {
        $createdUser = $this->user::create([
            'role' => 6,
            'email' => 'john@example.com',
            'password' => bcrypt('secret'),
        ]);
        $credentials = [
            'email' => $createdUser->email,
            'password' => 'secret'
        ];
        $this->withoutMiddleware()->post(route('auth-login'), $credentials);
        $this->assertAuthenticatedAs($createdUser);
    }
}