<?php

namespace Tests\Unit;

use App\Agreement;
use Tests\TestCase;
use App\User;
use App\UserAgreement;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AgreementTest extends TestCase
{
    use DatabaseMigrations;
    private $user, $agreement, $agreementUser;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->agreement = new Agreement();
        $this->agreementUser = new UserAgreement();
    }
    /**
     * check_if_user_has_agreed_to_a_property_term_and_condition
     * @test
     * @return void
     */
    public function check_if_user_has_agreed_to_any_term_and_condition()
    {
        $createUser = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 3);
        $agreementCreate = $this->agreement->createAgreement(
            $createUser->id,
            'this is the term and condition'
        );
        $this->agreementUser->addAgreement(
            $createUser->id,
            $agreementCreate->id
        );
        $agreementCount = $this->agreementUser->checkIfCustomerHasAgreed(
            $createUser->id,
            $agreementCreate->id
        );
        $this->assertEquals($agreementCount, 1);
    }

    /**
     * agreement_has_user
     * @test
     * @return void
     */
    public function agreement_has_user()
    {
        $createUser = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 3);

        $agreementCreate = $this->agreement->createAgreement(
            $createUser->id,
            'this is the term and condition'
        );

        $this->assertEquals($agreementCreate->user->email, $createUser->email);
    }
}