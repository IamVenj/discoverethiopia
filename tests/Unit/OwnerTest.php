<?php

namespace Tests\Unit;

use App\HotelOwner;
use App\Tour;
use App\TourLocation;
use App\TourOwner;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OwnerTest extends TestCase
{
    use DatabaseMigrations;
    private $hotelOwner, $user, $tour, $tourOwner, $tourLocation;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->hotelOwner = new HotelOwner();
        $this->tour = new Tour();
        $this->tourOwner = new TourOwner();
        $this->tourLocation = new TourLocation();
    }

    /**
     * owner_has_logo_and_check_if_user_is_accessible
     * @test
     * @return void
     */
    public function owner_has_logo_and_check_if_user_is_accessible()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdHotelOwner = $this->hotelOwner->createHotelOwner($createdOwner->id, 'description', 'https://cloudinary.com/yelp/propfile/njjiuisd.png', 'company', 24, 48, 24, 48);
        $this->assertNotEmpty($createdHotelOwner->logo);
        $this->assertEquals(
            $createdHotelOwner->user->email,
            $createdOwner->email
        );
    }


    /**
     * get_tours_by_tour_owner
     * @test relationship
     * @return void
     */
    public function get_tours_by_tour_owner()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdTourOwner = $this->tourOwner->createTourOwner($createdOwner->id, 'description', 'https://cloudinary.com/yelp/propfile/njjiuisd.png', 'company', 24, 48, 24, 48);
        $createdTourLocation = $this->tourLocation->createTourLocation('34.5555', '55.343', '43.4334', '34.5555');
        $createdTour = $this->tour->createTour('name', 'description', Carbon::createFromDate("12/24/2020"), 'departsFrom', 'arrivesAt', 300.00, $createdTourLocation->id, $createdOwner->id);
        $this->assertEquals(
            $createdTourOwner->tours[0]->name,
            $createdTour->name
        );
    }

    /**
     * owner_has_all_data_filled_properly
     * @test relationship
     * @return void
     */
    public function owner_has_all_data_filled_properly()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdHotelOwner = $this->hotelOwner->createHotelOwner($createdOwner->id, 'description', 'https://cloudinary.com/yelp/propfile/njjiuisd.png', 'company', 24, 48, 24, 48);
        $this->assertNotEmpty($createdHotelOwner->user_cancellation_refund_amount);
        $this->assertNotEmpty($createdHotelOwner->user_reschedule_time);
        $this->assertNotEmpty($createdHotelOwner->user_cancellation_time);
        $this->assertEquals(
            $createdHotelOwner->user_reschedule_refund_amount,
            $createdOwner->hotelOwners->user_reschedule_refund_amount
        );
        $this->assertEquals(
            $createdHotelOwner->user_cancellation_refund_amount,
            $createdOwner->hotelOwners->user_cancellation_refund_amount
        );
        $this->assertEquals(
            $createdHotelOwner->user_reschedule_time,
            $createdOwner->hotelOwners->user_reschedule_time
        );
        $this->assertEquals(
            $createdHotelOwner->user_cancellation_time,
            $createdOwner->hotelOwners->user_cancellation_time
        );
    }
}