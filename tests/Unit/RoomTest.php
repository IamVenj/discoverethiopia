<?php

namespace Tests\Unit;

use App\Amenity;
use App\HotelAmenity;
use App\HotelOwner;
use App\Review;
use App\Room;
use App\RoomImages;
use App\RoomType;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class RoomTest extends TestCase
{
    use DatabaseMigrations;
    private $room, $hotelOwner, $user, $roomType, $roomImage, $amenity, $hotelAmenity, $review;
    protected function setUp(): void
    {
        parent::setUp();
        $this->hotelOwner = new HotelOwner();
        $this->room = new Room();
        $this->user = new User();
        $this->amenity = new Amenity();
        $this->roomType = new RoomType();
        $this->roomImage = new RoomImages();
        $this->hotelAmenity = new HotelAmenity();
        $this->review = new Review();
    }
    /**
     * get_hotel_owner_by_room
     * @test relationship
     * @return void
     */
    public function get_hotel_owner_by_room()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $created_hotel_owner = $this->hotelOwner->createHotelOwner($createdOwner->id, 'description', 'logo', 'company', 48, 24, 48, 24);
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');
        $this->assertEquals(
            $createdRoom->hotelOwner->company_name,
            $created_hotel_owner->company_name
        );
    }

    /**
     * check_if_room_has_amenities
     * @test relationship
     * @return void
     */
    public function check_if_room_has_amenities()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $amenityCreatedForTest = $this->amenity->createAmenity('name', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $createdHotelAmenity = $this->hotelAmenity->addSingleAmenity(
            $amenityCreatedForTest->id,
            $createdRoom->id
        );
        $this->assertEquals(
            $createdRoom->amenities[0]->amenity_id,
            $createdHotelAmenity->amenity_id
        );
    }

    /**
     * get_reviews_via_room
     * @test relationship
     * @return void
     */
    public function get_reviews_via_room()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $createdReview = $this->review->createReview($createdCustomer->id, $createdOwner->id, 'this is the review', 5, null, null, $createdRoom->id, null);
        $this->assertEquals(
            $createdRoom->reviews[0]->review,
            $createdReview->review
        );
    }

    /**
     * get_room_type_via_room
     * @test relationship
     * @return void
     */
    public function get_room_type_via_room()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $this->assertEquals(
            $createdRoom->roomType->name,
            $createdRoomType->name
        );
    }

    /**
     * get_images_via_room
     * @test relationship
     * @return void
     */
    public function get_images_via_room()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $createdImage = $this->roomImage->singleAddImage(
            $createdRoom->id,
            ['signedUrl' => 'https://cloudinary.com/profile/bhjcdbjshcds.png']
        );

        $this->assertEquals(
            $createdRoom->images[0]->image,
            $createdImage->image
        );
    }
}