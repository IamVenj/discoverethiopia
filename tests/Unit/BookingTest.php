<?php

namespace Tests\Unit;

use App\Car;
use Tests\TestCase;
use App\CarBooking;
use App\CarCompanyOwner;
use App\HomeBooking;
use App\HomeOwner;
use App\HotelBooking;
use App\HotelOwner;
use App\Property;
use App\Room;
use App\RoomType;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BookingTest extends TestCase
{
    use DatabaseMigrations;

    private $carBooking, $homeBooking, $tourBooking, $hotelBooking;
    private $user, $room, $bookRoom, $hotelOwner, $roomType;
    private $car, $carOwner, $bookCar;
    private $property, $bookHome, $propertyOwner;
    protected function setUp(): void
    {
        parent::setUp();
        // any type of users
        $this->user = new User();

        // Hotels
        $this->room = new Room();
        $this->bookHotel = new HotelBooking();
        $this->hotelOwner = new HotelOwner();
        $this->roomType = new RoomType();

        // Cars
        $this->carOwner = new CarCompanyOwner();
        $this->car = new Car();
        $this->bookCar = new CarBooking();

        // guest house
        $this->property = new Property();
        $this->bookHome = new HomeBooking();
        $this->propertyOwner = new HomeOwner();
    }
    /**
     * refund_after_cancellation
     * @test
     * @return void
     */
    public function refund_after_cancellation()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $registerHotelBooking = $this->bookHotel->bookRoom($createdCustomer->id, $createdRoom->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 5, 2, 1, 'null', 'dewdewdew_njbb');
        $cancelBooking = $this->bookHotel->cancelBooking(1, $registerHotelBooking->id);

        $this->assertTrue(
            $cancelBooking->cancel == 1 && $cancelBooking->refund_status == 1
        );
    }

    /**
     * payment_is_executed
     * @test
     * @return void
     */
    public function payment_is_executed()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $registerHotelBooking = $this->bookHotel->bookRoom($createdCustomer->id, $createdRoom->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 5, 2, 1, 'null', 'dewdewdew_njbb');
        $this->assertTrue(
            $registerHotelBooking->payment_status == 1 && $registerHotelBooking->charge_id == 'dewdewdew_njbb'
        );
    }

    /**
     * can_the_user_cancel_after_time_limit_set_by_the_owners_is_up
     * @test
     * @return void
     */
    public function can_the_user_cancel_after_time_limit_set_by_the_owners_is_up()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $this->carOwner->createCarOwner($createdOwner->id, 'description', 'logo', 'company', 48, 24, 48, 24);

        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.00, 'available');
        $registerCarBooking = $this->bookCar->bookCar($createdCustomer->id, $createdCar->id, Carbon::createFromDate('2020', '01', '24'), Carbon::createFromDate('2020', '01', '25'), 'extra', '222222222222222222');

        $this->assertTrue(
            $this->bookCar->checkDeadlineForCancellation($registerCarBooking->id)
        );
    }

    /**
     * user_cannot_book_unavailable_car
     * @test
     * @return void
     */
    public function user_cannot_book_unavailable_car()
    {
        $createdCustomer1 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email1@gmail.com', '+251932382336', 'nationality', 6);
        $createdCustomer2 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email2@gmail.com', '+251932382336', 'nationality', 6);

        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.00, 'available');

        $this->bookCar->bookCar($createdCustomer1->id, $createdCar->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 'extra', '222222222222222222');
        $register_second_customer = $this->bookCar->bookCar($createdCustomer2->id, $createdCar->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 'extra', '222222222222222222');

        $assertion = null;
        $register_second_customer == null ?
            $assertion = true : $assertion = false;
        $this->assertTrue($assertion);
    }

    /**
     * user_cannot_book_unavailable_room
     * @test
     * @return void
     */
    public function user_cannot_book_unavailable_room()
    {
        $createdCustomer1 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email1@gmail.com', '+251932382336', 'nationality', 6);
        $createdCustomer2 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email2@gmail.com', '+251932382336', 'nationality', 6);

        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $this->bookHotel->bookRoom($createdCustomer1->id, $createdRoom->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 5, 2, 1, null, 'dewdewdew_njbb');
        $register_second_customer = $this->bookHotel->bookRoom($createdCustomer2->id, $createdRoom->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 5, 2, 1, null, 'dewdewdew_njbb2');

        $assertion = null;
        $register_second_customer == null ?
            $assertion = true : $assertion = false;
        $this->assertTrue($assertion);
    }

    /**
     * user_cannot_book_unavailable_guest_house
     * @test
     * @return void
     */
    public function user_cannot_book_unavailable_guest_house()
    {
        $createdCustomer1 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email1@gmail.com', '+251932382336', 'nationality', 6);
        $createdCustomer2 = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email2@gmail.com', '+251932382336', 'nationality', 6);

        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20,  2, 'basic layout description', $createdPropertyType->id);

        $this->bookHome->bookProperty($createdCustomer1->id, $createdProperty->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 2, 3, 0, 'dddddddddddddddddddd');
        $register_second_customer = $this->bookHome->bookProperty($createdCustomer2->id, $createdProperty->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 2, 3, 0, 'dddddddddddddddddddd');

        $assertion = null;
        $register_second_customer == null ?
            $assertion = true : $assertion = false;
        $this->assertTrue($assertion);
    }

    /**
     * user_booked_a_car_without_driver
     * assert That both values will be null
     * @test
     * @return void
     */
    public function user_booked_a_car_without_driver()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $this->carOwner->createCarOwner($createdOwner->id, 'description', 'logo', 'company', 48, 24, 48, 24);

        $createdCar = $this->car->createCar('name', 'model', 'description', $createdOwner->id, null, 200.0, 'available');
        $registerCarBooking = $this->bookCar->bookCar($createdCustomer->id, $createdCar->id, Carbon::createFromDate('10/28/2020'), Carbon::createFromDate('12/28/2020'), 'extra', '222222222222222222');
        $this->assertEquals(
            $registerCarBooking->car->driver,
            $createdCar->driver
        );
    }
}