<?php

namespace Tests\Unit;

use App\Address;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\User;

class AddressTest extends TestCase
{
    use DatabaseMigrations;
    private $user, $address;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->address = new Address();
    }
    /**
     * address_has_a_user
     * @test
     * @return void
     */
    public function address_has_a_user()
    {
        $_user = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $_address = $this->address->addAddress(
            $_user->id,
            1.2333,
            3.45556,
            "edna mall"
        );
        $this->assertNotEmpty($_address->user_id);
        $this->assertEquals($_address->user_id, $_user->id);
        $this->assertEquals($_address->user->email, $_user->email);
    }
}