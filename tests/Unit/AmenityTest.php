<?php

namespace Tests\Unit;

use App\Amenity;
use App\HomeAmenity;
use App\HotelAmenity;
use App\Property;
use App\Room;
use App\RoomType;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AmenityTest extends TestCase
{
    use DatabaseMigrations;
    private $amenityFactory, $amenity, $hotelAmenity, $room, $roomType, $owner, $homeAmenity, $property;
    protected function setUp(): void
    {
        parent::setUp();
        $this->amenity = new Amenity();
        $this->hotelAmenity = new HotelAmenity();
        $this->room = new Room();
        $this->roomType = new RoomType();
        $this->owner = new User();
        $this->homeAmenity = new HomeAmenity();
        $this->property = new Property();
        $this->amenityFactory = factory(Amenity::class)->make();
    }
    /**
     * amenity_has_name
     * @test
     * @return void
     */
    public function amenity_has_name()
    {
        $this->assertNotEmpty($this->amenityFactory->name);
    }

    /**
     * amenity_has_type
     * @test
     * @return void
     */
    public function amenity_has_type()
    {
        $this->assertNotEmpty($this->amenityFactory->type);
    }

    /**
     * get_room_and_amenity_by_hotel_amenity
     * @test relationship
     * @return void
     */
    public function get_room_and_amenity_by_hotel_amenity()
    {
        $amenityCreatedForTest = $this->amenity->createAmenity('name', 'hotel');

        $createdOwner = $this->owner->registerOwners('email@gmail.com', bcrypt('password'), 2);
        $createdRoomType = $this->roomType->createRoomType('name', 'description', 'hotel');
        $createdRoom = $this->room->createRoom(200.00, $createdOwner->id, 10, $createdRoomType->id, 'available', 5, 2, 'description about bed', '50', 2, 'description about room', 'allowed');

        $createdHotelAmenity = $this->hotelAmenity->addSingleAmenity(
            $amenityCreatedForTest->id,
            $createdRoom->id
        );

        $this->assertEquals(
            $createdHotelAmenity->amenities->name,
            $amenityCreatedForTest->name
        );
        $this->assertEquals(
            $createdHotelAmenity->room[0]->name,
            $createdRoom->name
        );
    }

    /**
     * get_properties_and_amenity_by_guest_house_amenity
     * @test relationship
     * @return void
     */
    public function get_properties_and_amenity_by_guest_house_amenity()
    {
        $amenityCreatedForTest = $this->amenity->createAmenity('name', 'home');

        $createdOwner = $this->owner->registerOwners('email@gmail.com', bcrypt('password'), 3);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $createdGuestHouseAmenity = $this->homeAmenity->addSingleAmenity(
            $amenityCreatedForTest->id,
            $createdProperty->id
        );

        $this->assertEquals(
            $createdGuestHouseAmenity->amenities->name,
            $amenityCreatedForTest->name
        );
        $this->assertEquals(
            $createdGuestHouseAmenity->properties[0]->name,
            $createdProperty->name
        );
    }
}