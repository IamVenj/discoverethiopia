<?php

namespace Tests\Unit;

use App\Amenity;
use App\HomeAmenity;
use App\HomeOwner;
use App\Property;
use App\PropertyImage;
use App\Review;
use App\RoomType;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PropertyTest extends TestCase
{
    use DatabaseMigrations;
    private $user, $roomType, $propertyOwner, $propertyAmenity;
    private $amenity, $review, $propertyImage, $property;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
        $this->roomType = new RoomType();
        $this->propertyAmenity = new HomeAmenity();
        $this->amenity = new Amenity();
        $this->propertyOwner = new HomeOwner();
        $this->propertyImage = new PropertyImage();
        $this->review = new Review();
        $this->property = new Property();
    }
    /**
     * get_home_owner_by_property
     * @test relationship
     * @return void
     */
    public function get_home_owner_by_property()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdHomeOwner = $this->propertyOwner->createHomeOwner($createdOwner->id, 'description', 'logo', 'company', 24, 48, 48, 50);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');

        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $this->assertEquals(
            $createdProperty->homeOwner->company_name,
            $createdHomeOwner->company_name
        );
    }

    /**
     * check_if_property_has_amenities
     * @test relationship
     * @return void
     */
    public function check_if_property_has_amenities()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');

        $createdAmenity = $this->amenity->createAmenity('name', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $createdPropertyAmenity = $this->propertyAmenity->addSingleAmenity(
            $createdAmenity->id,
            $createdProperty->id
        );

        $this->assertEquals(
            $createdPropertyAmenity->properties[0]->name,
            $createdProperty->name
        );
    }

    /**
     * get_reviews_via_property
     * @test relationship
     * @return void
     */
    public function get_reviews_via_property()
    {
        $createdCustomer = $this->user->registerCH('firstname', 'lastname', bcrypt('${1:my-secret-password'), 'email@gmail.com', '+251932382336', 'nationality', 1);
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $createdReview = $this->review->createReview($createdCustomer->id, $createdOwner->id, 'this is the review', 5, null, null, null, $createdProperty->id);
        $this->assertEquals(
            $createdProperty->reviews[0]->review,
            $createdReview->review
        );
    }

    /**
     * get_property_type_via_property
     * @test relationship
     * @return void
     */
    public function get_property_type_via_property()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $this->assertEquals(
            $createdProperty->propertyType->name,
            $createdPropertyType->name
        );
    }

    /**
     * get_images_via_property
     * @test relationship
     * @return void
     */
    public function get_images_via_property()
    {
        $createdOwner = $this->user->registerOwners('email@example.com', bcrypt('password'), 2);
        $createdPropertyType = $this->roomType->createRoomType('name', 'description', 'home');
        $createdProperty = $this->property->createProperty('propertyName', 'Some Description', 150.00, $createdOwner->id, 'Bole Medhanialem, lalala', 5.344444, 42.55555555555, 'available', 12, 2, 'description about bed', 20, 2, 'basic layout description', $createdPropertyType->id);

        $createdImage = $this->propertyImage->singleAddImage(
            $createdProperty->id,
            ['signedUrl' => 'https://cloudinary.com/profile/bhjcdbjshcds.png']
        );
        $this->assertEquals(
            $createdProperty->images[0]->image,
            $createdImage->image
        );
    }
}