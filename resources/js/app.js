/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VModal from 'vue-js-modal'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import * as VueGoogleMaps from 'vue2-google-maps'
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import wysiwyg from "vue-wysiwyg";
import "vue-wysiwyg/dist/vueWysiwyg.css";
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import vueCountryRegionSelect from 'vue-country-region-select'
import TimeAgo from 'vue2-timeago'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import VueTabs from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'
import StarRating from 'vue-star-rating'
import RangeSlider from 'vue-range-slider'
import 'vue-range-slider/dist/vue-range-slider.css'
import DateRangePicker from 'vue2-daterange-picker'
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'

window.Vue = require('vue');
Vue.use(VModal)
Vue.use(VueSweetalert2);
Vue.use(VueFormWizard)
Vue.component('range-slider', RangeSlider)
Vue.component('date-range-picker', DateRangePicker)
Vue.use(wysiwyg, { hideModules: { "image": true } });
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyC3YkZNNySdyR87o83QEHWglHfHD_PZqiw',
        libraries: 'places'
    }
});
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('vue2Dropzone', vue2Dropzone);
Vue.component('timeago', TimeAgo);
Vue.use(vueCountryRegionSelect)
Vue.use(VueAwesomeSwiper)
Vue.use(VueTabs)
Vue.component('star-rating', StarRating);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/*
|----------------------------------------------------------------------------------------------
|	Main Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('navigation-component', require('./components/pre-login/index/nav/nav.vue').default);
Vue.component('theme-options', require('./components/pre-login/index/theme-options/theme-options.vue').default);
Vue.component('login-signup-theme', require('./components/pre-login/index/theme-options/login&signup-option.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Home Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('hotel-form', require('./components/pre-login/pages/home/hotel-form.vue').default);
Vue.component('cars-form', require('./components/pre-login/pages/home/cars-form.vue').default);
Vue.component('flight-form', require('./components/pre-login/pages/home/flight-form.vue').default);
Vue.component('tour-form', require('./components/pre-login/pages/home/tour-form.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Flight Form Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('one-way', require('./components/pre-login/pages/home/flight-forms/one-way.vue').default);
Vue.component('roundtrip', require('./components/pre-login/pages/home/flight-forms/roundtrip.vue').default);
Vue.component('multicity', require('./components/pre-login/pages/home/flight-forms/multicity.vue').default);


/*
|----------------------------------------------------------------------------------------------
|	Auth Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('login-component', require('./components/pre-login/auth/LoginComponent.vue').default);
Vue.component('buyer-signup', require('./components/pre-login/auth/buyer-signup.vue').default);
Vue.component('car-signup', require('./components/pre-login/auth/car-signup.vue').default);
Vue.component('hotel-signup', require('./components/pre-login/auth/hotel-signup.vue').default);
Vue.component('tour-signup', require('./components/pre-login/auth/tour-signup.vue').default);
Vue.component('homeowner-component', require('./components/pre-login/auth/HomeOwnerComponent.vue').default);
Vue.component('wizard-component', require('./components/pre-login/auth/wizardComponent.vue').default);
Vue.component('user-map', require('./components/google/MapComponent.vue').default);
Vue.component('show-map-component', require('./components/google/ShowMapComponent.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Partial Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('banner', require('./components/pre-login/pages/banner.vue').default);
Vue.component('sort', require('./components/pre-login/pages/sort.vue').default);
Vue.component('load-more', require('./components/pre-login/pages/load-more.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Hotel Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('hotel-filter', require('./components/pre-login/pages/hotel/filter.vue').default);
Vue.component('hotel-card', require('./components/pre-login/pages/hotel/hotel-card.vue').default);
Vue.component('hoteldetail-component', require('./components/pre-login/pages/hotel/hotelDetailComponent.vue').default);
Vue.component('hotel-room-detail-component', require('./components/pre-login/pages/hotel/hotelRoomDetailComponent.vue').default);

/**
 * Home Components
 */
Vue.component('home-filter-component', require('./components/pre-login/pages/vacationalHome/filter.vue').default);
Vue.component('home-guset-card-component', require('./components/pre-login/pages/vacationalHome/home-card.vue').default);
Vue.component('home-detail-component', require('./components/pre-login/pages/vacationalHome/HomeDetailComponent.vue').default);


/*
|----------------------------------------------------------------------------------------------
|	Flight Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('flight-card', require('./components/pre-login/pages/flight/flight-card.vue').default);
Vue.component('flight-filter', require('./components/pre-login/pages/flight/filter.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Car Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('car-filter', require('./components/pre-login/pages/cars/filter.vue').default);
Vue.component('car-card', require('./components/pre-login/pages/cars/car-card.vue').default);
Vue.component('car-detail-component', require('./components/pre-login/pages/cars/CarDetailComponent').default);

/*
|----------------------------------------------------------------------------------------------
|	Car Components
|----------------------------------------------------------------------------------------------
*/

Vue.component('tour-guest-card-component', require('./components/pre-login/pages/tour/tour-card.vue').default);
Vue.component('tour-filter', require('./components/pre-login/pages/tour/filter.vue').default);
Vue.component('tour-detail-component', require('./components/pre-login/pages/tour/TourDetailComponent.vue').default);

/*
|----------------------------------------------------------------------------------------------
|	Agreement Components
|----------------------------------------------------------------------------------------------
*/
Vue.component('agreement-component', require('./components/pre-login/pages/AgreementComponent.vue').default);
Vue.component('book-component', require('./components/pre-login/pages/booking/BookComponent.vue').default);
Vue.component('thankyou-component', require('./components/pre-login/pages/thankyou/ThankyouComponent.vue').default);


/**
 * -------------------------------------------------------------
 * Post Component
 * -------------------------------------------------------------
 */
Vue.component('footer-component', require('./components/post/app/footerComponent.vue').default);

/**
 * Admin Components
 * -----------------------------------------------------
 */
Vue.component('admin-amenity-component', require('./components/post/pages/admin/amenity/AmenityComponent.vue').default);
Vue.component('admin-agreement-component', require('./components/post/pages/admin/agreement/AgreementComponent.vue').default);
Vue.component('admin-room-type-component', require('./components/post/pages/admin/roomtype/RoomTypeComponent.vue').default);
Vue.component('admin-user-component', require('./components/post/pages/admin/users/UserComponent.vue').default);
Vue.component('admin-user-show-component', require('./components/post/pages/admin/users/ShowUserComponent.vue').default);

Vue.component('admin-booking-component', require('./components/post/pages/admin/booking/BookingComponent.vue').default);
Vue.component('admin-show-booking-component', require('./components/post/pages/admin/booking/ShowOwnerBookingComponent.vue').default);


/**
 * Car Components
 * -----------------------------------------------------
 */

Vue.component('add-car-component', require('./components/post/pages/car/cars/CarComponent.vue').default);
Vue.component('car-image-component', require('./components/post/pages/car/cars/CarImageComponent.vue').default);
Vue.component('create-car-component', require('./components/post/pages/car/cars/CreateCarComponent.vue').default);
Vue.component('edit-car-component', require('./components/post/pages/car/cars/EditCarComponent.vue').default);
Vue.component('car-booking-component', require('./components/post/pages/car/booking/BookingComponent.vue').default);
Vue.component('car-agreement-component', require('./components/post/pages/car/agreement/AgreementComponent.vue').default);
Vue.component('add-driver-component', require('./components/post/pages/car/drivers/CreateDriverComponent.vue').default);

/**
 * Tour Components
 * -----------------------------------------------------
 */
Vue.component('list-tour-component', require('./components/post/pages/tour/tours/TourComonent.vue').default);
Vue.component('add-tour-component', require('./components/post/pages/tour/tours/CreateTourComponent.vue').default);
Vue.component('edit-tour-component', require('./components/post/pages/tour/tours/EditTourComponent.vue').default);
Vue.component('tour-booking-component', require('./components/post/pages/tour/booking/BookingComponent.vue').default);
Vue.component('tour-agreement-component', require('./components/post/pages/tour/agreement/AgreementComponent.vue').default);

/**
 * Hotel Components
 * -----------------------------------------------------
 */
Vue.component('hotel-room-component', require('./components/post/pages/hotel/rooms/RoomsComponent.vue').default);
Vue.component('hotel-room-image-component', require('./components/post/pages/hotel/rooms/RoomsImageComponent.vue').default);
Vue.component('create-hotel-room-component', require('./components/post/pages/hotel/rooms/CreateRoomsComponent.vue').default);
Vue.component('edit-hotel-room-component', require('./components/post/pages/hotel/rooms/EditRoomComponent.vue').default);
Vue.component('hotel-booking-component', require('./components/post/pages/hotel/booking/BookingComponent.vue').default);
Vue.component('hotel-agreement-component', require('./components/post/pages/hotel/agreement/AgreementComponent.vue').default);

/**
 * Home Components
 * -----------------------------------------------------
 */

Vue.component('home-property-component', require('./components/post/pages/home/property/PropertyComponent.vue').default);
Vue.component('create-home-property-component', require('./components/post/pages/home/property/CreatePropertyComponent.vue').default);
Vue.component('edit-property-component', require('./components/post/pages/home/property/EditPropertyComponent.vue').default);
Vue.component('home-booking-component', require('./components/post/pages/home/booking/BookingComponent.vue').default);
Vue.component('home-agreement-component', require('./components/post/pages/home/agreement/AgreementComponent.vue').default);
Vue.component('property-image-component', require('./components/post/pages/home/property/PropertyImageComponent.vue').default);

/**
* Next, we will create a fresh Vue application instance and attach it to
* the page. Then, you may begin adding components to this application
* or customize the JavaScript scaffolding to fit your unique needs.
*/

Vue.component('image-update-component', require('./components/post/pages/modal/imageModal.vue').default);
Vue.component('delete-component', require('./components/post/pages/modal/deleteModal.vue').default);

Vue.component('edit-amenity-component', require('./components/post/pages/admin/modals/amenity/editModal.vue').default);
Vue.component('edit-roomtype-component', require('./components/post/pages/admin/modals/roomtype/editModal.vue').default);

const app = new Vue({
    el: '#app',
});
