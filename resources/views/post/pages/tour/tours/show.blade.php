@extends('pre-login.index.index')

@section('content')
<div data-section-type="default" class="inner-banner style-5"
    style="@if(count($tour->images) != 0) background-image: url({{ asset($tour->images[0]->image) }}); @else background-image: url({{ asset('client/img/home_6/slide_8.jpg') }}); @endif background-size: cover; background-repeat: no-repeat; background-position: center center; background-attachment: scroll;">
    <div class="dark-layer"></div>
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <ul class="banner-breadcrumb color-white clearfix" style="margin-top: 80px;">
                    <li><a class="link-blue-2" href="/">Home</a> /</li>
                    <li><a class="link-blue-2" href="/">Tours</a> /</li>
                    <li style="cursor: default;">{{ $tour->tourOwner->company_name }}</li>
                </ul>

                <h2 class="color-white">{{ $tour->name }}</h2>
                <h4 class="color-white">{{ $tour->date }} </h4>
            </div>
        </div>
    </div>
</div>
<tour-detail-component id="{{ $tour->id }} "></tour-detail-component>
@include('pre-login.index.footer')
@endsection