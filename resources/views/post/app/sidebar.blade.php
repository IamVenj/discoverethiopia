<aside class="main-sidebar sidebar-primary elevation-4">
    <a href="/" class="brand-link">
        <img src="{{ asset('client/img/logo.png') }}" alt="Discover Ethiopia Logo" class="brand-image"
            style="opacity: .8">
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('post/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info" style=" width: 100%;">
                @if(Auth::user()->role == 1 || Auth::user()->role == 3 || Auth::user()->role == 6)
                {{ Auth::user()->firstname.' '.Auth::user()->lastname }}
                @elseif(Auth::user()->role == 2)
                {{ Auth::user()->hotelOwners->company_name }}
                @elseif(Auth::user()->role == 4)
                {{ Auth::user()->carCompanyOwners->company_name }}
                @elseif(Auth::user()->role == 5)
                {{ Auth::user()->tourOwners->company_name }}
                @endif
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="/dashboard" class="nav-link">
                        <i class="fas fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                @if(Auth::user()->role == 1)

                <li class="nav-item">
                    <a href="/admin/amenity" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Amenities</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/admin/agreement" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Agreement</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/admin/roomtype" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Room Type</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/admin/users" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/admin/booking" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>View Booking</p>
                    </a>
                </li>

                @endif

                @if(Auth::user()->role == 2)
                <li class="nav-item">
                    <a href="/hotel/rooms" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Rooms</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/hotel/booking" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>Manage Booking</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/hotel/agreement" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Agreement</p>
                    </a>
                </li>
                @endif


                @if(Auth::user()->role == 3)

                <li class="nav-item">
                    <a href="/home/property" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Properties</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/home/booking" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>Manage Booking</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/home/agreement" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Agreement</p>
                    </a>
                </li>

                @endif

                @if(Auth::user()->role == 4)

                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-car"></i>
                        <p>
                            Cars
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/car/cars" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>Cars</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/car/driver" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Drivers</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="/car/booking" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>
                            Manage Booking
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/car/agreement" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Agreement
                        </p>
                    </a>
                </li>

                @endif

                @if(Auth::user()->role == 5)

                <li class="nav-item">
                    <a href="/tour/tours" class="nav-link">
                        <i class="nav-icon fas fa-bus"></i>
                        <p>
                            Tours
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/tour/booking" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>
                            Manage Booking
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/tour/agreement" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>
                            Agreement
                        </p>
                    </a>
                </li>

                @endif

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
