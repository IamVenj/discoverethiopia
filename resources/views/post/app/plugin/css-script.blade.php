<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Discover Ethiopia</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{csrf_token()}}" />
<link rel="stylesheet" href="{{ asset('post/plugins/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('post/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/plugins/jqvmap/jqvmap.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/dist/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('post/plugins/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('post/plugins/summernote/summernote-bs4.css') }}">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,700" rel="stylesheet">
