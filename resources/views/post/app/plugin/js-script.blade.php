<script src="{{ asset('post/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('post/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
    function handlePreloader() {
        if ($(".total-lds").length) {
            $(".lds-grid")
                .delay(200)
                .fadeOut(500);
            $(".total-lds")
                .delay(200)
                .fadeOut(500);
        }
    }
    $(window).on("load", function() {
        handlePreloader();
    });
</script>
<script src="{{ asset('post/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('post/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('post/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('post/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('post/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('post/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('post/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('post/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('post/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('post/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('post/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('post/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('post/dist/js/pages/dashboard.js') }}"></script>
<script src="{{ asset('post/dist/js/demo.js') }}"></script>
