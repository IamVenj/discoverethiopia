<!DOCTYPE html>
<html>

<head>
    @include('post.app.plugin.css-script')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    @include('pre-login.index.loader')
    <div class="wrapper" id="app">
        @include('post.app.nav')
        @include('post.app.sidebar')
        @yield('content')
        @include('post.app.footer')
    </div>
    <script src="https://js.stripe.com/v3/"></script>
    @include('post.app.plugin.js-script')
</body>

</html>
