@extends('pre-login.index.index')

@section('content')

<div class="video-wrapper" style="z-index: 2000;">
	<!-- <div class="video-overlay"></div> -->
	<img class="center-image" src="img/special/bg-2.jpg" alt="">
	<div class="not-found" >     
		<div class="not-found-box">
			<div class="not-found-title">ouch!</div>
			<div class="not-found-message">sorry the page you are looking for does not exist</div>
			<a href="/" class="c-button bg-white hv-white-o"><span><i class="fa fa-arrow-left" style="margin-right: 10px;"></i>Go Home</span></a>
		</div>
	</div>
</div>
        

@endsection