@extends('pre-login.index.index')
@section('content')
<banner name="Cars" name_count="940" image="{{URL::asset('client/img/special/bg-3.jpg')}}"></banner>
<div class="list-wrapper bg-grey-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <car-filter></car-filter>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-9">
                <sort></sort>
                <div class="grid-content clearfix">
                    <car-card></car-card>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pre-login.index.footer')
@endsection
