@extends('pre-login.index.index')
@section('content')
<banner name="Hotels" name_count="640" image="{{ asset('client/img/special/bg-1.jpg') }}"></banner>
<div class="list-wrapper bg-grey-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <hotel-filter></hotel-filter>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-9">
                <sort></sort>
                <div class="grid-content clearfix">
                    <hotel-card></hotel-card>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pre-login.index.footer')
@endsection
