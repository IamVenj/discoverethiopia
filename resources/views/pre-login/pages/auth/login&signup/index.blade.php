@extends('pre-login.index.index')

@section('content')

<div class="video-wrapper">
    <img class="center-image" src="{{ asset('client/img/special/bg-1.jpg') }}" alt="">
    <div class="container">
        <div class="simple-tab type-2 tab-wrapper">
            <div class="tab-nav-wrapper" style="position: absolute; top: 20%;">
                <div class="nav-tab  clearfix">
                    <div class="nav-tab-item active">
                        Login
                    </div>

                    <div class="nav-tab-item">
                        Signup as a customer
                    </div>

                    <div class="nav-tab-item">
                        as a hotel manager
                    </div>

                    <div class="nav-tab-item">
                        as a home owner
                    </div>

                    <div class="nav-tab-item">
                        as a cars dealer
                    </div>

                    <div class="nav-tab-item">
                        as a tour manager
                    </div>

                </div>
            </div>

            <div class="tabs-content clearfix">
                <login-component></login-component>
                <buyer-signup></buyer-signup>
                <hotel-signup></hotel-signup>
                <homeowner-component></homeowner-component>
                <car-signup></car-signup>
                <tour-signup></tour-signup>
            </div>
        </div>
        <div class="full-copy">© <?= date('Y');?> All rights reserved. Discover Ethiopia</div>
    </div>
</div>

@endsection
