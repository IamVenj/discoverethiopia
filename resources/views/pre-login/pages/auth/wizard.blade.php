<!DOCTYPE html>
<html lang="en">

<head>
    @include('pre-login.index.plugin.css-script')
</head>

<body>
    @include('pre-login.index.loader')
    <div class="video-wrapper">
        <div class="overlay"></div>
        <img class="center-image" src="{{ asset('client/img/special/bg-1.jpg') }}" alt="">
        <div class="container" id="app"
            style="display: flex; justify-content: center; flex-direction: row; height: 100%;">
            @include('pre-login.index.theme-options')
            <div class="card"
                style="width: 100%; z-index:2; border-radius:20px; margin-top: 50px; margin-bottom: 50px; box-shadow:0 10px 10px 5px rgba(0,0,0,0.07); background: #fff;">
                <div style="margin-bottom: 30px;"></div>
                <img src="{{ asset('client/img/logo.png') }}"
                    style="height: 70px; display: flex; justify-content: center; margin: auto;">
                <div style="margin-bottom: 30px;"></div>
                <wizard-component></wizard-component>
            </div>

        </div>
    </div>
    <script src="https://js.stripe.com/v3/"></script>
    @include('pre-login.index.plugin.js-script')
</body>

</html>
