@extends('pre-login.index.index')
@section('content')

<banner name="Guest Houses" name_count="640" image="{{ asset('client/img/special/bg-1.jpg') }}"></banner>

<div class="list-wrapper bg-grey-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <home-filter-component></home-filter-component>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-9">
                <sort></sort>
                <div class="grid-content clearfix">
                    <home-guset-card-component></home-guset-card-component>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pre-login.index.footer')
@endsection
