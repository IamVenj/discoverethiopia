@extends('pre-login.index.index')

@section('content')
<div data-section-type="default" class="inner-banner style-5"
    style="@if(count($property->images) != 0) background-image: url({{ asset($property->images[0]->image) }}); @else background-image: url({{ asset('client/img/home_6/slide_8.jpg') }}); @endif background-size: cover; background-repeat: no-repeat; background-position: center center; background-attachment: scroll;">
    <div class="dark-layer"></div>
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <ul class="banner-breadcrumb color-white clearfix" style="margin-top: 80px;">
                    <li><a class="link-blue-2" href="/">Home</a> /</li>
                    <li><a class="link-blue-2" href="/">Guest House</a> /</li>
                    <li style="cursor: default;">{{ $property->homeOwner->company_name }}</li>
                </ul>

                <h2 class="color-white">{{ $property->name }}</h2>
                <h4 class="color-white">{{ $property->address }} </h4>
            </div>
        </div>
    </div>
</div>
<home-detail-component id="{{ $property->id }} "></home-detail-component>
@include('pre-login.index.footer')
@endsection
