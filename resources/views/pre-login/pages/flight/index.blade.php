@extends('pre-login.index.index')

@section('content')


<banner name="Flights" name_count="800" image="{{URL::asset('client/img/home_3/main_slide_1.jpg')}}"></banner>


<div class="list-wrapper bg-grey-2">

    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-sm-4 col-md-3">

                <flight-filter></flight-filter>

            </div>

            <div class="col-xs-12 col-sm-8 col-md-9">

                <sort></sort>

                <div class="grid-content clearfix">

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_1.jpg') }}" flight_type="one way"
                        price="$860" flight_location="paris" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_2.jpg') }}" flight_type="one way"
                        price="$460" flight_location="london" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_3.jpg') }}" flight_type="one way"
                        price="$560" flight_location="virginia, United States" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_4.jpg') }}" flight_type="one way"
                        price="$760" flight_location="Canada" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_1.jpg') }}" flight_type="one way"
                        price="$660" flight_location="Morroco" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_2.jpg') }}" flight_type="one way"
                        price="$260" flight_location="Japan" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_3.jpg') }}" flight_type="one way"
                        price="$360" flight_location="India" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                    <flight-card image="{{ asset('client/img/tour_list/flight_grid_4.jpg') }}" flight_type="one way"
                        price="$460" flight_location="Las Vegas, United States" take_off_date="wed nov 13, 2013 7:50 am"
                        landing_date="wed nov 13, 2013 7:50 am"></flight-card>

                </div>

                <load-more></load-more>

            </div>

        </div>

    </div>

</div>

@include('pre-login.index.footer')

@endsection
