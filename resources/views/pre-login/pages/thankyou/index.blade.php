@extends('pre-login.index.index')
@section('content')
<div class="inner-banner style-5"
    style="background-image:url({{ asset('client/img/bg14.jpg') }});background-size:cover; background-repeat:no-repeat;background-position:center center;background-attachment:scroll;"
    data-section-type="default">
    <div class="dark-layer" style=""></div>
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2 class="color-white" style="margin-top: 80px;">Thank you</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<thankyou-component></thankyou-component>
@endsection
