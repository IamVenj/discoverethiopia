@extends('pre-login.index.index')

@section('content')

<div class="full-height">
    <div class="video-wrapper">
        <div class="video-overlay"></div>

        <video loop autoplay muted class="bgvid" id="bgvid">
            <source type="video/mp4" src="{{ asset('client/video/video.mp4') }}">
            <source type="video/ogv" src="{{ asset('client/video/video.ogv') }}">
            <source type="video/webm" src="{{ asset('client/video/video.webm') }}">
        </video>

        <div class="vertical-align">
            <div class="container">
                <div class="tabs-slider">
                    <div class="baner-tabs">
                        <div class="text-center">
                            <div class="drop-tabs">
                                <ul class="nav-tabs tpl-tabs tabs-style-1">
                                    <li class="active click-tabs">
                                        <a href="#two" data-toggle="tab" aria-expanded="false">
                                            <span class="fa fa-plane"></span>
                                            flights
                                        </a>
                                    </li>

                                    <li class="click-tabs">
                                        <a href="#one" data-toggle="tab" aria-expanded="false">
                                            <span class="fa fa-bed"></span>
                                            hotels
                                        </a>
                                    </li>

                                    <li class="click-tabs">
                                        <a href="#three" data-toggle="tab" aria-expanded="false">
                                            <span class="fa fa-car"></span>
                                            cars
                                        </a>
                                    </li>

                                    <li class="click-tabs">
                                        <a href="#four" data-toggle="tab" aria-expanded="false">
                                            <span class="fa fa-ship"></span>
                                            Tours
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="tab-content tpl-tabs-cont section-text t-con-style-1">

                            <hotel-form></hotel-form>
                            <flight-form></flight-form>
                            <cars-form></cars-form>
                            <tour-form></tour-form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @guest
        <a href="/auth" class="btn login-btn">LOGIN & SIGN UP</a>
        @endguest

    </div>
</div>

@endsection
