@if(!\Request::is('/') && !\Request::is('auth'))
@guest <login-signup-theme></login-signup-theme> @endguest
@endif
@auth
<div class="style-page">
    <div class="wrappers">
        <div class="conf-button">
            <span class="fa fa-cog"></span>

            <h6>options</h6>
        </div>

        <a href="/" class="site-logo">

            @auth
            @if(Auth::user()->photo == null)
            <img src="{{ asset('client/img/logo.png') }}" alt height="70" />
            @else
            <img src="{{ Auth::user()->photo }}" alt
                style="border-radius: 50%; height: 70px; width: 70px; object-fit:cover;" />
            @endif
            @else
            <img src="{{ asset('client/img/logo.png') }}" alt height="70" />
            @endauth
        </a>

        {{-- <hr />

        <div class="color-block">
            <h5>Language</h5>

            <select class="form-control">
                <option>English | ENG</option>
                <option>Amharic | AMH</option>
            </select>
        </div>

        <hr /> --}}


        <hr />
        @auth
        @if (Auth::user()->role == 6)
        <a class="color-block" style="text-align: left; text-transform: uppercase; font-weight: 100; font-size: 14px;"
            href="#">
            <i class="fa fa-edit" style="margin-right: 10px;"></i>
            Account Setttings
        </a>
        <hr />
        @endif
        @if (Auth::user()->role == 2)
        @if (!is_null(Auth::user()->phone_number) && Auth::user()->hotelOwners()->count() > 0)
        <a class="color-block" style="text-align: left; text-transform: uppercase; font-weight: 100; font-size: 14px;"
            href="#">
            <i class="fa fa-edit" style="margin-right: 10px;"></i>
            Account Setttings
        </a>
        <hr />
        @endif
        @endif
        @if (Auth::user()->role == 3)
        @if (Auth::user()->homeOwners()->count() > 0)
        <a class="color-block" style="text-align: left; text-transform: uppercase; font-weight: 100; font-size: 14px;"
            href="#">
            <i class="fa fa-edit" style="margin-right: 10px;"></i>
            Account Setttings
        </a>
        <hr />
        @endif
        @endif
        @if (Auth::user()->role == 4)
        @if (!is_null(Auth::user()->phone_number) && Auth::user()->carCompanyOwners()->count() > 0)
        <a class="color-block" style="text-align: left; text-transform: uppercase; font-weight: 100; font-size: 14px;"
            href="#">
            <i class="fa fa-edit" style="margin-right: 10px;"></i>
            Account Setttings
        </a>
        <hr />
        @endif
        @endif
        @if (Auth::user()->role == 5)
        @if (!is_null(Auth::user()->phone_number) && Auth::user()->homeOwners()->count() > 0)
        <a class="color-block" style="text-align: left; text-transform: uppercase; font-weight: 100; font-size: 14px;"
            href="#">
            <i class="fa fa-edit" style="margin-right: 10px;"></i>
            Account Setttings
        </a>
        <hr />
        @endif
        @endif


        <form action="/auth/logout" method="post">
            @csrf
            <button class="color-block" type="submit"
                style="text-align: left; margin-top:12px; padding-left: 0px; text-transform: uppercase; font-weight: 700; font-size: 14px;">
                <i class="fa fa-lock" style="margin-right: 10px;"></i>
                Logout
            </button>
        </form>

        <hr />
        @endauth
    </div>
</div>
@endauth
