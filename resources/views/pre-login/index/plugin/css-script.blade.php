<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no" />
<meta name="csrf-token" content="{{csrf_token()}}" />
<link rel="shortcut icon" href="favicon.ico" />
<link href="{{URL::asset('client/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('client/css/jquery-ui.structure.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('client/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('client/css/DateTimePicker.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{URL::asset('client/fonts/font-awesome/4.3.0/css/font-awesome.min.css')}}">
<link href="{{URL::asset('client/css/style.css')}}" rel="stylesheet" type="text/css" />
<title>Discover Ethiopia</title>
