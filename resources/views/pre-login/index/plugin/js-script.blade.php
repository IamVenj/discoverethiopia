<script src="{{URL::asset('client/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{URL::asset('js/app.js')}}"></script>
<script src="{{URL::asset('client/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('client/js/jquery-ui.min.js')}}"></script>
<script src="{{URL::asset('client/js/idangerous.swiper.min.js')}}"></script>
<script src="{{URL::asset('client/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{URL::asset('client/js/isotope.pkgd.min.js')}}"></script>
<script src="{{URL::asset('client/js/DateTimePicker.min.js')}}"></script>
<script src="{{URL::asset('client/js/jquery.mousewheel.min.js')}}"></script>
<script src="{{URL::asset('client/js/jquery.circliful.min.js')}}"></script>
<script src="{{URL::asset('client/js/jquery.countTo.js')}}"></script>
<script src="{{URL::asset('client/js/all.js')}}"></script>

<script>
    function handlePreloader() {
        if ($(".total-lds").length) {
            $(".lds-grid")
                .delay(200)
                .fadeOut(500);
            $(".total-lds")
                .delay(200)
                .fadeOut(500);
        }
    }
    $(window).on("load", function() {
        handlePreloader();
    });
</script>
