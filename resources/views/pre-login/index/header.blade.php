<header class="type-3 color-9 hovered scrol-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="nav">

                    <a href="/" class="logo">
                        <img src="{{ asset('client/img/logo.png') }}" style="height: 70px;" />
                    </a>

                    <div class="nav-menu-icon">
                        <a href="#"><i></i></a>
                    </div>

                    <nav class="menu" style="margin-top: 30px;">
                        <navigation-component>
                        </navigation-component>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
