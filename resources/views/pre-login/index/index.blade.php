<!DOCTYPE html>
<html>

<head>
    @include('pre-login.index.plugin.css-script')
</head>

<body data-color="theme-2" class="noborder">
    @include('pre-login.index.loader')
    <div id="app">

        @include('pre-login.index.theme-options')
        @include('pre-login.index.header')
        @yield('content')

    </div>
    <script src="https://js.stripe.com/v3/"></script>
    @include('pre-login.index.plugin.js-script')
</body>

</html>
