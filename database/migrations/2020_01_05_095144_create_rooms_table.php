<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hotel_owner_id')->unsigned();
            $table->foreign('hotel_owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('room_no');
            $table->bigInteger('room_type_id')->unsigned();
            $table->integer('maximum_capacity');
            $table->double('price_per_night');
            $table->integer('bedroom');
            $table->text('bed_detail');
            $table->string('square_feet');
            $table->string('availability');
            $table->integer('bathroom');
            $table->longText('basic_layout');
            $table->string('smoking_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}