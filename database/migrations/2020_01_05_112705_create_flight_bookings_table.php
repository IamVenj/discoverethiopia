<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('flight_name');
            $table->string('flying_from');
            $table->date('departing_date');
            $table->date('arrival_date');
            $table->string('flight_type');
            $table->integer('adults')->default(1);
            $table->integer('children')->default(0);
            $table->integer('infant')->default(0);
            $table->boolean('cancel')->default(0);
            $table->boolean('refund_status')->default(0);
            $table->boolean('payment_status')->default(1);
            $table->string('charge_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_bookings');
    }
}