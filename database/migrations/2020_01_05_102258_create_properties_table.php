<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description');
            $table->string('price_per_night');
            $table->bigInteger('home_owner_id')->unsigned();
            $table->foreign('home_owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('availability');
            $table->integer('maximum_capacity');
            $table->integer('bedroom');
            $table->text('bed_detail');
            $table->string('square_feet');
            $table->bigInteger('property_type')->unsigned();
            $table->foreign('property_type')->references('id')->on('room_types')->onDelete('cascade');
            $table->integer('bathroom');
            $table->longText('basic_layout');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}