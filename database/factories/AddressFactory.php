<?php

/** @var \Illuminate\Database\Eloquent\AddressFactory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Address::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'address' => $faker->word,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
    ];
});