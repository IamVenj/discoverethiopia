<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use App\AdminSettings;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'role' => 1
    ];
});

$factory->define(\App\Address::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'address' => $faker->word,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
    ];
});

$factory->define(AdminSettings::class, function (Faker $faker) {
    return [
        'hotel_agreement' => $faker->name,
        'tour_agreement' => $faker->name,
        'car_agreement' => $faker->name,
        'customer_agreement' => $faker->name,
        'home_agreement' => $faker->name,
    ];
});