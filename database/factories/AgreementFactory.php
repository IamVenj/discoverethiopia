<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Agreement;
use Faker\Generator as Faker;

$factory->define(Agreement::class, function (Faker $faker) {
    return [
        'user+id' => $faker->number,
        'terms_and_conditions' => $faker->words
    ];
});