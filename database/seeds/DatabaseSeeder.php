<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(App\User::class)->create([
            'firstname' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('discoverAdmin'),
            'role' => '1',
            'phone_number' => '+25190898767',
            'nationality' => 'Ethiopian'
        ]);

        DB::table('admin_settings')->delete();
        factory(App\AdminSettings::class)->create([
            'hotel_agreement' => 'dummy data',
            'tour_agreement' => 'dummy data',
            'car_agreement' => 'dummy data',
            'customer_agreement' => 'dummy data',
            'home_agreement' => 'dummy data',
        ]);
    }
}