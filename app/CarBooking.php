<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Car;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CarBooking extends Model
{
    protected $fillable = [
        'car_id', 'booked_from', 'booked_until', 'payment_status', 'refund_status',
        'cancel', 'user_id', 'expired', 'extra_info', 'charge_id'
    ];
    protected $hidden = ['user_id', 'car_id'];


    /**
     * Relationship between car and user
     * @return object
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * Relationship between user and booking
     * @return object
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function identifyBookedPropertyInTimeInterval($checkin, $checkout, $carId)
    {
        return $this->where('checkin', '>=', $checkin)->where('checkout', '<=', $checkout)->where('car_id', $carId)->count();
    }

    public function getCarBooks($carId)
    {
        return $this->where('car_id', $carId)->get();
    }

    /**
     * @param string $car
     * @param date $bookedFrom
     * @param date $bookedUntil
     * @param string $extra_info
     * @param string $chargeId
     * @return void
     */
    public function bookCar($user_id, $carId, $bookedFrom, $bookedUntil, $extra_info, $chargeId)
    {
        $car = new Car();
        $book = null;
        if ($this->identifyBookedPropertyInTimeInterval($bookedFrom, $bookedUntil, $carId) == 0) {
            $book = $this->book($user_id, $carId, $bookedFrom, $bookedUntil, $extra_info, $chargeId);
            $car->setAvailability($carId, 'unavailable');
        }
        return $book;
    }

    public function book($user_id, $car, $bookedFrom, $bookedUntil, $extra_info, $chargeId)
    {
        $book = $this::create([
            'car_id' => $car,
            'booked_from' => $bookedFrom,
            'booked_until' => $bookedUntil,
            'extra_info' => $extra_info,
            'user_id' => $user_id,
            'charge_id' => $chargeId,
            'payment_status' => 1
        ]);
        return $book;
    }

    /**
     * We subtract the current date with the booked date to retrieve the gap of the booking,
     * or when to check when it was booked!
     * @param int $currentDate
     * @param int $bookedDate
     * @return int
     */
    public function calculateDate($currentDate, $bookedDate)
    {
        $difference = strtotime($currentDate) - strtotime($bookedDate);
        $hours = round($difference / (60 * 60));
        return $hours;
    }


    /**
     * @param int $booking
     * @return int
     */
    public function getCarOwnersCancellationDeadline($booking)
    {
        return $booking->car->carOwner->user_cancellation_time;
    }

    /**
     * @param int $booking
     * @return int
     */
    public function getCarOwnersRescheduleDeadline($booking)
    {
        return $booking->car->carOwner->user_reschedule_time;
    }

    /**
     * @param int $carId
     * @return object
     */

    public function expiredBooking($carId)
    {
        $booking = $this::with(['car'])->whereBetween('booked_from')->where('car_id', $carId)->where('user_id', Auth::user()->id)->get();
    }

    /**
     * get Current Expiry status
     * @param int
     * @return bool
     */
    public function currentExpiryStatus($id)
    {
        $booking = $this::find($id)->where('booked_until', '<=', Carbon::now())->count();
        return $booking == 1 ? true : false;
    }

    /**
     * This is a bool method to check if the deadline has passed.
     * The method takes in id of the current booking, then we calculate the current date with created date.
     * if the owners deadline is greater than calculatedDate it will return true otherwise its always false.
     * the owners deadline is not a date its an integer based on hours for example: 48.
     * That is why we are able to compare the deadline with the calculated date.
     * The dates are DateType that are turned by strtotime() php code!
     * Therefore we are able to check who is greater!
     * @param int $id
     * @return bool
     */

    public function checkDeadlineForCancellation($id)
    {
        $currentDate = Carbon::now();
        $booking = $this::with(['car'])->where('expired', 0)->find($id);
        $calculatedDate = $this->calculateDate($currentDate, $booking->created_at);
        if ($this->getCarOwnersCancellationDeadline($booking) > $calculatedDate) {
            return true;
        }
        return false;
    }


    public function checkDeadlineForReschedule($id)
    {
        $currentDate = Carbon::now();
        $booking = $this::with(['car'])->where('expired', 0)->find($id);
        $calculatedDate = $this->calculateDate($currentDate, $booking->created_date);
        if ($this->getCarOwnersRescheduleDeadline($booking) > $calculatedDate) {
            return true;
        }
        return false;
    }


    /**
     * an event will be triggered to send an email to the user!
     * Payment will be executed!
     * whilst refunding its required to update the availability of the booked component!!!
     * or maybe whilst cancelling
     * @param int $refundStatus
     * @param int $id
     * @return void
     */
    public function refundPayment($refundStatus, $id)
    {
        $book = $this::find($id);
        $book->refund_status = $refundStatus;
        $book->save();
    }

    /**
     * @param int $cancel
     * @param int $id
     * @return void
     */
    public function cancelBooking($cancel, $id)
    {
        if ($this->checkDeadlineForCancellation($id) && $this->currentExpiryStatus($id)) {
            $this->refundPayment(1, $id);
            $book = $this::find($id);
            $book->cancel = $cancel;
            $book->save();
        }
    }
}