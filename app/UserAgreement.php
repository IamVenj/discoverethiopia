<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Agreement;
use Illuminate\Support\Facades\Auth;

class UserAgreement extends Model
{
    /**
     * This Model represents the signed agreement between the owners
     * and the client who is using their service.
     */

    protected $fillable = ['client_id', 'agreement_id'];
    protected $hidden = ['client_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'client_id');
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class, 'id', 'agreement_id');
    }

    public function checkIfCustomerHasAgreed($clientId, $agreementId)
    {
        return $this::where('client_id', $clientId)->where('agreement_id', $agreementId)->count();
    }

    public function addAgreement($clientId, $agreementId)
    {
        $agreement = null;
        if ($this->checkIfCustomerHasAgreed($clientId, $agreementId) == 0) {
            $agreement = $this::create([
                'client_id' => $clientId,
                'agreement_id' => $agreementId
            ]);
        }
        return $agreement;
    }

    /**
     * Not going to be implemented now
     */
    public function removeAgreement()
    {
    }
}