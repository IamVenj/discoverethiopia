<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tour;

class TourImage extends Model
{
    protected $fillable = ['tour_id', 'image'];
    protected $hidden = ['tour_id'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
    public function addImages($tour, $images)
    {
        for ($i = 0; $i < count($images); $i++) {
            $this::create([
                'tour_id' => $tour,
                'image' => $images[$i]['signedUrl']
            ]);
        }
    }

    public function updateImage($imageId, $image)
    {
        $tourImage = $this::find($imageId);
        $tourImage->image = $image;
        $tourImage->save();
    }

    public function destroyImages($id)
    {
        $this::find($id)->delete();
    }
}