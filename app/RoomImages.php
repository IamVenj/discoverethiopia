<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;

class RoomImages extends Model
{
    protected $fillable = ['image', 'room_id'];
    protected $hidden = ['room_id'];

    public function room()
    {
        $this->belongsTo(Room::class);
    }

    public function addImages($room, $images)
    {
        for ($i = 0; $i < count($images); $i++) {
            $this::create([
                'room_id' => $room,
                'image' => $images[$i]['signedUrl']
            ]);
        }
    }

    public function getRoomImages($room_id)
    {
        return $this->where('room_id', $room_id)->get();
    }

    public function singleAddImage($room, $image)
    {
        $image = $this::create([
            'room_id' => $room,
            'image' => $image['signedUrl']
        ]);
        return $image;
    }

    public function updateImage($imageId, $image)
    {
        $roomImage = $this::find($imageId);
        $roomImage->image = $image;
        $roomImage->save();
    }

    public function destroyImage($id)
    {
        $this::find($id)->delete();
    }
}