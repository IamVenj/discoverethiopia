<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Car;

class CarImage extends Model
{
    protected $fillable = ['car_id', 'image'];
    protected $hidden = ['car_id'];

    public function car()
    {
        return $this->belongsTo(Car::class, 'id', 'car_id');
    }

    public function addImages($car, $images)
    {
        for ($i = 0; $i < count($images); $i++) {
            $this::create([
                'car_id' => $car,
                'image' => $images[$i]['signedUrl']
            ]);
        }
    }

    /**
     * This function os for testing
     * @test required
     * --------------
     */
    public function addSingleImage($car, $image)
    {
        $carImage = $this::create([
            'car_id' => $car,
            'image' => $image['signedUrl']
        ]);
        return $carImage;
    }

    public function updateImage($imageId, $image)
    {
        $carImage = $this::find($imageId);
        $carImage->image = $image;
        $carImage->save();
    }

    public function destroyImages($id)
    {
        $this::find($id)->delete();
    }
}