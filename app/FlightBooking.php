<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Auth;

class FlightBooking extends Model
{
    protected $fillable = [
        'user_id', 'flight_name', 'flying_from', 'departing_date', 'arrival_date',
        'flight_type', 'adults', 'children', 'infant', 'cancel', 'refund_status',
        'payment_status', 'charge_id', 'seat_no'
    ];
    protected $hidden = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * we need records of total seats and seat number taken
     * flight classes - business class | economy class
     * - plane seats model
     * || seat number
     * || airplane id
     */

    public function identifyBookedPropertyInTimeInterval($checkin, $checkout, $flightId)
    {
    }

    public function bookFlight(
        $flightName,
        $flyingFrom,
        $departingDate,
        $arrivalDate,
        $flightType,
        $adults,
        $children,
        $infant,
        $chargeId
    ) {
        $this::create([
            'user_id' => Auth::user()->id,
            'flight_name' => $flightName,
            'flying_from' => $flyingFrom,
            'departing_date' => $departingDate,
            'arrival_date' => $arrivalDate,
            'flight_type' => $flightType,
            'adults' => $adults,
            'children' => $children,
            'infant' => $infant,
            'charge_id' => $chargeId
        ]);
    }
}