<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Property;

class PropertyImage extends Model
{
    protected $fillable = ['image', 'property_id'];
    protected $hidden = ['property_id'];

    public function property()
    {
        $this->belongsTo(Property::class);
    }

    public function getPropertyImages($property_id)
    {
        return $this->where('property_id', $property_id)->get();
    }

    public function addImages($property, $images)
    {
        for ($i = 0; $i < count($images); $i++) {
            $this::create([
                'property_id' => $property,
                'image' => $images[$i]['signedUrl']
            ]);
        }
    }

    public function singleAddImage($property, $image)
    {
        $image = $this::create([
            'property_id' => $property,
            'image' => $image['signedUrl']
        ]);
        return $image;
    }

    public function updateImage($imageId, $image)
    {
        $propertyImage = $this::find($imageId);
        $propertyImage->image = $image;
        $propertyImage->save();
    }

    public function destroyImage($id)
    {
        $this::find($id)->delete();
    }
}