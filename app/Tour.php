<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TourLocation;
use App\TourOwner;
use App\TourImage;
use App\TourBooking;
use App\Review;

class Tour extends Model
{
    /**
     * arrives at and departs from will be changed with google map
     * which might require another table
     * -------------------------------------------------------------
     * Update will proceed after final project is concluded!
     * ----------------------------------------------------------------
     * number of people who can tour on the tour
     */
    protected $fillable = ['name', 'description', 'tour_date', 'departs_from', 'arrives_at', 'price_per_person', 'tour_location_id', 'tour_owner_id', 'total_people'];
    protected $hidden = ['tour_owner_id'];

    public function tourLocation()
    {
        return $this->belongsTo(TourLocation::class);
    }

    public function tourOwner()
    {
        return $this->belongsTo(TourOwner::class, 'user_id', 'tour_owner_id');
    }

    public function toursOwner()
    {
        return $this->hasOne(TourOwner::class, 'user_id', 'tour_owner_id');
    }

    public function images()
    {
        return $this->hasMany(TourImage::class, 'tour_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(TourBooking::class, 'tour_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'tour_id', 'id');
    }

    public function recoupDataById($id)
    {
        return $this::with(['tourOwner.user', 'tourLocation'])->find($id);
    }

    public function setAvailability($id, $availability)
    {
        $property = $this::find($id);
        $property->availability = $availability;
        $property->save();
    }

    public function checkAvailability($id)
    {
        return $this::find($id)->availability;
    }

    public function createTour(
        $name,
        $description,
        $tourDate,
        $departsFrom,
        $arrivesAt,
        $price,
        $tourLocationId,
        $ownerId
    ) {
        $tours = $this::create([
            'name' => $name,
            'description' => $description,
            'tour_date' => $tourDate,
            'departs_from' => $departsFrom,
            'arrives_at' => $arrivesAt,
            'price_per_person' => $price,
            'tour_location_id' => $tourLocationId,
            'tour_owner_id' => $ownerId
        ]);
        return $tours;
    }

    public function updateTour(
        $id,
        $name,
        $description,
        $tourDate,
        $departsFrom,
        $arrivesAt,
        $tourLocationId,
        $price,
        $ownerId
    ) {
        $tour = $this::find($id);
        $tour->name = $name;
        $tour->description = $description;
        $tour->tour_date = $tourDate;
        $tour->departs_from = $departsFrom;
        $tour->tour_location_id = $tourLocationId;
        $tour->arrives_at = $arrivesAt;
        $tour->price_per_person = $price;
        $tour->tour_owner_id = $ownerId;
        $tour->save();
    }

    public function destroyTour($id)
    {
        $this::find($id)->delete();
    }
}