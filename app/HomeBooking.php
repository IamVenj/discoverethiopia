<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Property;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeBooking extends Model
{
    protected $fillable = [
        'property_id', 'checkin', 'checkout', 'adults', 'children',
        'infants', 'user_id', 'payment_status', 'refund_status', 'cancel',
        'extra_info', 'charge_id'
    ];
    protected $hidden = ['user_id', 'property_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function getUsers()
    {
        return $this::with(['user', 'property'])->latest()->paginate(10);
    }

    public function createBooking($userId, $property_id, $checkin, $checkout, $adults, $children, $infants, $chargeId)
    {
        $booking = $this::create([
            'property_id' => $property_id,
            'checkin' => $checkin,
            'checkout' => $checkout,
            'adults' => $adults,
            'children' => $children,
            'infants' => $infants,
            'user_id' => $userId,
            'charge_id' => $chargeId
        ]);
        return $booking;
    }

    public function getPropertyBooks($propertyId)
    {
        return $this->where('property_id', $propertyId)->get();
    }

    public function identifyBookedPropertyInTimeInterval($checkin, $checkout, $property_id)
    {
        return $this->where('checkin', '>=', $checkin)->where('checkout', '<=', $checkout)->where('property_id', $property_id)->count();
    }

    public function bookProperty($userId, $property_id, $checkin, $checkout, $adults, $children, $infants, $chargeId)
    {
        $property = new Property();
        $book = null;
        if ($this->identifyBookedPropertyInTimeInterval($checkin, $checkout, $property_id) == 0) {
            $book = $this->createBooking($userId, $property_id, $checkin, $checkout, $adults, $children, $infants, $chargeId);
            $property->setAvailability($property_id, 'unavailable');
        }
        return $book;
    }

    public function refundPayment($refundStatus, $id)
    {
        $book = $this::find($id);
        $book->refund_status = $refundStatus;
        /**
         * an event will be triggered to send an email to the user!
         * Payment will be executed!
         */
        $book->save();
    }

    public function cancelBooking($cancel, $id)
    {
        $this->refundPayment(1, $id);
        $book = $this::find($id);
        $book->cancel = $cancel;
        $book->save();
    }
}