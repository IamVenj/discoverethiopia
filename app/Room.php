<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RoomType;
use App\HotelOwner;
use App\RoomImages;
use App\HotelAmenity;
use App\HotelBooking;
use App\Review;

class Room extends Model
{
    /**
     * Room is required for HOTELS who have multiple rooms
     * They create rooms one after another
     */
    protected $fillable = [
        'hotel_owner_id', 'room_no', 'room_type_id', 'maximum_capacity', 'price_per_night', 'availability',
        'bedroom', 'bed_detail', 'square_feet', 'bathroom', 'basic_layout', 'smoking_status'
    ];

    public function roomType()
    {
        return $this->belongsTo(RoomType::class);
    }

    public function hotelOwner()
    {
        return $this->hasOne(HotelOwner::class, 'user_id', 'hotel_owner_id');
    }

    public function images()
    {
        return $this->hasMany(RoomImages::class);
    }

    public function amenities()
    {
        return $this->hasMany(HotelAmenity::class, 'room_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(HotelBooking::class, 'room_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'room_id', 'id');
    }

    public function setAvailability($id, $availability)
    {
        $room = $this::find($id);
        $room->availability = $availability;
        $room->save();
    }

    public function checkAvailability($id)
    {
        return $this::find($id)->availability;
    }

    public function createRoom(
        $price,
        $ownerId,
        $roomNo,
        $roomTypeId,
        $availability,
        $max_capacity,
        $bedroom,
        $bed_detail,
        $square_feet,
        $bathroom,
        $basic_layout,
        $smokingStatus
    ) {
        $room = $this::create([
            'room_no' => $roomNo,
            'room_type_id' => $roomTypeId,
            'price_per_night' => $price,
            'hotel_owner_id' => $ownerId,
            'availability' => $availability,
            'maximum_capacity' => $max_capacity,
            'bedroom' => $bedroom,
            'bed_detail' => $bed_detail,
            'square_feet' => $square_feet,
            'bathroom' => $bathroom,
            'basic_layout' => $basic_layout,
            'smoking_status' => $smokingStatus
        ]);
        return $room;
    }

    public function updateRoom(
        $id,
        $price,
        $ownerId,
        $roomNo,
        $roomTypeId,
        $availability,
        $max_capacity,
        $bedroom,
        $bed_detail,
        $square_feet,
        $bathroom,
        $basic_layout,
        $smokingStatus
    ) {
        $room = $this::find($id);
        $room->room_no = $roomNo;
        $room->room_type_id = $roomTypeId;
        $room->price_per_night = $price;
        $room->hotel_owner_id = $ownerId;
        $room->availability = $availability;
        $room->maximum_capacity = $max_capacity;
        $room->bedroom = $bedroom;
        $room->bed_detail = $bed_detail;
        $room->square_feet = $square_feet;
        $room->bathroom = $bathroom;
        $room->basic_layout = $basic_layout;
        $room->smoking_status = $smokingStatus;
        $room->save();
    }

    public function destroyRoom($id)
    {
        $this::find($id)->delete();
    }

    public function recoupDataById($id)
    {
        return $this::with(['hotelOwner.user', 'roomType'])->find($id);
    }
}