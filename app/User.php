<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\CarCompanyOwner;
use App\HomeOwner;
use App\HotelOwner;
use App\TourOwner;
use App\Address;
use App\CarBooking;
use App\FlightBooking;
use App\HotelBooking;
use App\HomeBooking;
use App\TourBooking;
use App\Review;
use App\Rating;
use Illuminate\Support\Facades\Auth;

/**
 * There are a total of 6 user roles which are as follows
 * The assigned numbers on the roles are the numbers we will use to implement
 * on the code.
 * -------------------------------------------------------
 * admin - 1
 * The admin basically creates amenities, agreement, analyze user,
 * create roomType for both hotel anf property eg. luxury and such. | registered users | manage account | manage booking
 * ---------------------------------------------
 * hotel - 2
 * The hotel creates rooms, view booked rooms, the users that booked the room, manage booking, create agreement
 * ----------------------------------------------
 * home - 3
 * The home role creates property, add amenities to property, manage booking, agreement
 * ----------------------------------------------
 * car - 4
 * The car role creates cars, manage bookings, agreement, creates drivers
 * -------------------------------------------------
 * tour - 5
 * The tour role creates tours, manage bookings, agreement
 * --------------------------------------------------
 * client - 6
 * manage Booking, search, filter, manage account
 * ---------------------------------------------------
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'photo', 'role', 'phone_number', 'phone2', 'phone3', 'nationality'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tourOwners()
    {
        return $this->hasOne(TourOwner::class);
    }

    public function carCompanyOwners()
    {
        return $this->hasOne(CarCompanyOwner::class);
    }

    public function homeOwners()
    {
        return $this->hasOne(HomeOwner::class);
    }

    public function hotelOwners()
    {
        return $this->hasOne(HotelOwner::class, 'user_id', 'id');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function carBooking()
    {
        return $this->hasMany(CarBooking::class);
    }

    public function flightBooking()
    {
        return $this->hasMany(FlightBooking::class);
    }

    public function tourBooking()
    {
        return $this->hasMany(TourBooking::class);
    }

    public function homeBooking()
    {
        return $this->hasMany(HomeBooking::class);
    }

    public function hotelBooking()
    {
        return $this->hasMany(HotelBooking::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function registerCH($firstname, $lastname, $password, $email, $phone, $nationality, $role)
    {
        $user = $this::create([
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password,
            'email' => $email,
            'role' => $role,
            'phone_number' => $phone,
            'nationality' => $nationality
        ]);
        return $user;
    }

    public function getUserWithEmail($email)
    {
        return $this->where('email', $email);
    }


    public function updateUserWizard($photo, $phone1, $phone2, $phone3)
    {
        $user = $this::find(Auth::user()->id);
        $user->photo = $photo;
        $user->phone_number = $phone1;
        $user->phone2 = $phone2;
        $user->phone3 = $phone3;
        $user->save();
        return $user;
    }

    public function registerOwners($email, $password, $role)
    {
        $owner = $this::create([
            'email' => $email,
            'password' => $password,
            'role' => $role
        ]);
        return $owner;
    }
}