<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Car;
use Illuminate\Support\Facades\Auth;

class CarCompanyOwner extends Model
{
    protected $fillable = [
        'user_id', 'description', 'logo', 'company_name',
        'user_cancellation_time', 'user_reschedule_time',
        'user_cancellation_refund_amount', 'user_reschedule_refund_amount'
    ];

    protected $hidden = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function agreement()
    {
        return $this->hasOne(Agreement::class, 'user_id', 'user_id');
    }

    public function createCarOwner(
        $userId,
        $description,
        $logo,
        $companyName,
        $userCancellationTime,
        $userRescheduleTime,
        $userCancellationRefundAmount,
        $userRescheduleRefundAmount
    ) {
        $owner = $this::create([
            'user_id' => $userId,
            'description' => $description,
            'logo' => $logo,
            'company_name' => $companyName,
            'user_cancellation_time' => $userCancellationTime,
            'user_reschedule_time' => $userRescheduleTime,
            'user_cancellation_refund_amount' => $userCancellationRefundAmount,
            'user_reschedule_refund_amount' => $userRescheduleRefundAmount
        ]);
        return $owner;
    }

    public function updateCarOwner(
        $description,
        $logo,
        $companyName,
        $userCancellationTime,
        $userRescheduleTime,
        $userCancellationRefundAmount,
        $userRescheduleRefundAmount
    ) {
        $carOwner = $this::where('user_id', Auth::user()->id)->first();
        $carOwner->description = $description;
        $carOwner->company_name = $companyName;
        $carOwner->logo = $logo;
        $carOwner->user_cancellation_time = $userCancellationTime;
        $carOwner->user_reschedule_time = $userRescheduleTime;
        $carOwner->user_cancellation_refund_amount = $userCancellationRefundAmount;
        $carOwner->user_reschedule_refund_amount = $userRescheduleRefundAmount;
        $carOwner->save();
    }
}