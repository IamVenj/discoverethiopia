<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourLocation extends Model
{
    protected $fillable = ['departs_from_latitude', 'departs_from_longitude', 'arrives_at_latitude', 'arrives_at_longitude'];

    public function createTourLocation($dLat, $dLng, $aLat, $aLng)
    {
        $location = $this::create([
            'departs_from_latitude' => $dLat,
            'departs_from_longitude' => $dLng,
            'arrives_at_latitude' => $aLat,
            'arrives_at_longitude' => $aLng
        ]);
        return $location;
    }

    public function updateLocation($dLat, $dLng, $aLat, $aLng, $id)
    {
        $location = $this::find($id);
        $location->departs_from_latitude = $dLat;
        $location->departs_from_longitude = $dLng;
        $location->arrives_at_latitude = $aLat;
        $location->arrives_at_longitude = $aLng;
        $location->save();
    }

    public function destroyLocation($id)
    {
        $this::find($id)->delete();
    }
}