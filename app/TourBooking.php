<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Tour;
use Illuminate\Support\Facades\Auth;

class TourBooking extends Model
{
    protected $fillable = [
        'user_id', 'tour_id', 'payment_status',
        'refund_status', 'cancel', 'expired', 'extra_info', 'charge_id'
    ];
    protected $hidden = ['user_id', 'tour_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function getTourBooks($tourId)
    {
        return $this->where('tour_id', $tourId)->get();
    }

    public function bookTour($tour_id, $extra_info, $charge_id)
    {
        $tour = new Tour();
        $_tour = null;
        if ($tour->checkAvailability($tour_id) == 'available') {
            $_tour = $this::create([
                'user_id' => Auth::user()->id,
                'tour_id' => $tour_id,
                'extra_info' => $extra_info,
                'charge_id' => $charge_id
            ]);
            $tour->setAvailability($tour_id, 'unavailable');
        }
        return $_tour;
    }

    public function refundPayment($refundStatus, $id)
    {
        $book = $this::find($id);
        $book->refund_status = $refundStatus;
        /**
         * an event will be triggered to send an email to the user!
         * Payment will be executed!
         */
        $book->save();
    }

    public function cancelBooking($cancel, $id)
    {
        $this->refundPayment(1, $id);
        $book = $this::find($id);
        $book->cancel = $cancel;
        $book->save();
    }
}