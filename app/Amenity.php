<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    /**
     * type - is for either Hotel Amenity or Home Amenity or Both
     */
    protected $fillable = ['name', 'type'];

    public function createAmenity($name, $type)
    {
        $amenity = $this::create([
            'name' => $name,
            'type' => $type
        ]);
        return $amenity;
    }

    public function updateAmenity($id, $name, $type)
    {
        $amenity = $this::find($id);
        $amenity->name = $name;
        $amenity->type = $type;
        $amenity->save();
    }

    public function destroyAmenity($id)
    {
        $this::find($id)->delete();
    }
}