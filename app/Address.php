<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Auth;

class Address extends Model
{
    protected $fillable = ['user_id', 'address', 'latitude', 'longitude'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addAddress($userId, $lat, $lng, $name)
    {
        $address = null;
        if ($userId != "" && $lat != "" && $lng != "" && $name != "")
            $address = $this::create([
                'user_id' => $userId,
                'address' => $name,
                'latitude' => $lat,
                'longitude' => $lng
            ]);
        return $address;
    }


    public function createAddress($address)
    {
        for ($i = 0; $i < count($address); $i++) {
            $lat = $address[$i]['geometry']['location']['lat'];
            $lng = $address[$i]['geometry']['location']['lng'];
            $addressName = $address[$i]['name'] . ", " . $address[$i]['formatted_address'];
            $this->addAddress(Auth::user()->id, $lat, $lng, $addressName);
        }
    }

    public function updateAddress($address, $latitude, $longitude, $id)
    {
        $addressU = $this::find($id);
        $addressU->address = $address;
        $addressU->latitude = $latitude;
        $addressU->longitude = $longitude;
        $addressU->save();
    }

    public function destroyAddress($id)
    {
        $this::find($id)->delete();
    }
}