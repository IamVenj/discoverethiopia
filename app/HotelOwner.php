<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;
use Illuminate\Support\Facades\Auth;
use App\Agreement;

class HotelOwner extends Model
{
    protected $fillable = [
        'user_id', 'description', 'logo', 'company_name',
        'user_cancellation_time', 'user_reschedule_time',
        'user_cancellation_refund_amount', 'user_reschedule_refund_amount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class, 'hotel_owner_id', 'user_id');
    }

    public function agreement()
    {
        return $this->hasOne(Agreement::class, 'user_id', 'user_id');
    }

    public function createHotelOwner(
        $userId,
        $description,
        $logo,
        $company,
        $user_cancellation_time,
        $user_reschedule_time,
        $user_cancellation_refund_amount,
        $user_reschedule_refund_amount
    ) {
        $owner = $this::create([
            'user_id' => $userId,
            'description' => $description,
            'logo' => $logo,
            'company_name' => $company,
            'user_cancellation_time' => $user_cancellation_time,
            'user_reschedule_time' => $user_reschedule_time,
            'user_cancellation_refund_amount' => $user_cancellation_refund_amount,
            'user_reschedule_refund_amount' => $user_reschedule_refund_amount
        ]);
        return $owner;
    }

    public function updateHotelOwner(
        $description,
        $logo,
        $companyName,
        $userCancellationTime,
        $userRescheduleTime,
        $userCancellationRefundAmount,
        $userRescheduleRefundAmount
    ) {
        $hotelOwner = $this::where('user_id', Auth::user()->id)->first();
        $hotelOwner->description = $description;
        $hotelOwner->company_name = $companyName;
        $hotelOwner->logo = $logo;
        $hotelOwner->user_cancellation_time = $userCancellationTime;
        $hotelOwner->user_reschedule_time = $userRescheduleTime;
        $hotelOwner->user_cancellation_refund_amount = $userCancellationRefundAmount;
        $hotelOwner->user_reschedule_refund_amount = $userRescheduleRefundAmount;
        $hotelOwner->save();
    }
}