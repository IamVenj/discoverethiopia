<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;
use App\User;
use Illuminate\Support\Facades\Auth;

class HotelBooking extends Model
{
    protected $fillable = [
        'room_id', 'checkin', 'checkout', 'adults', 'children',
        'infants', 'user_id', 'payment_status', 'refund_status', 'cancel',
        'charge_id', 'extra_info'
    ];
    protected $hidden = ['user_id', 'room_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // public function recoupUnavailableRoom()
    // {
    //     return $this->where('room_id', )
    // }

    /**
     * @param date
     * @param date
     * @return object
     */
    public function recoupUnavailable($checkin, $checkout)
    {
        return $this->whereDate('checkin', '<=', $checkin)->where('checkout', '>=', $checkout)->get();
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function getUsers()
    {
        return $this::with(['user', 'room'])->latest()->paginate(10);
    }

    public function getRoomBooks($roomId)
    {
        return $this->where('room_id', $roomId)->get();
    }

    public function identifyBookedPropertyInTimeInterval($checkin, $checkout, $room_id)
    {
        return $this->where('checkin', '>=', $checkin)->where('checkout', '<=', $checkout)->where('room_id', $room_id)->count();
    }

    public function bookRoom($userId, $roomId, $checkin, $checkout, $adults, $children, $infants, $extra_info, $charge_id)
    {
        $room = new Room();
        $hotel = null;
        if ($this->identifyBookedPropertyInTimeInterval($checkin, $checkout, $roomId) == 0) {
            $hotel = $this->createBooking($userId, $roomId, $checkin, $checkout, $adults, $children, $infants, $extra_info, $charge_id);
            $room->setAvailability($roomId, 'unavailable');
        }
        return $hotel;
    }

    public function createBooking($userId, $roomId, $checkin, $checkout, $adults, $children, $infants, $extra_info, $charge_id)
    {
        $hotel = $this::create([
            'room_id' => $roomId,
            'checkin' => $checkin,
            'checkout' => $checkout,
            'adults' => $adults,
            'children' => $children,
            'infants' => $infants,
            'user_id' => $userId,
            'payment_status' => 1,
            'charge_id' => $charge_id,
            'extra_info' => $extra_info
        ]);
        return $hotel;
    }

    public function refundPayment($refundStatus, $id)
    {
        $book = $this::find($id);
        $book->refund_status = $refundStatus;
        /**
         * an event will be triggered to send an email to the user!
         * Payment will be executed!
         */
        $book->save();
    }

    public function cancelBooking($cancel, $id)
    {
        $this->refundPayment($cancel, $id);
        $book = $this::find($id);
        $book->cancel = $cancel;
        $book->save();
        return $book;
    }
}