<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Property;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Agreement;

class HomeOwner extends Model
{
    protected $fillable = [
        'user_id', 'description', 'logo', 'company_name',
        'user_cancellation_time', 'user_reschedule_time',
        'user_cancellation_refund_amount', 'user_reschedule_refund_amount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'home_owner_id', 'user_id');
    }

    public function agreement()
    {
        return $this->hasOne(Agreement::class, 'user_id', 'user_id');
    }

    public function createHomeOwner(
        $userId,
        $description,
        $logo,
        $company,
        $user_cancellation_time,
        $user_reschedule_time,
        $user_cancellation_refund_amount,
        $user_reschedule_refund_amount
    ) {
        $owner = $this::create([
            'user_id' => $userId,
            'description' => $description,
            'logo' => $logo,
            'company_name' => $company,
            'user_cancellation_time' => $user_cancellation_time,
            'user_reschedule_time' => $user_reschedule_time,
            'user_cancellation_refund_amount' => $user_cancellation_refund_amount,
            'user_reschedule_refund_amount' => $user_reschedule_refund_amount
        ]);
        return $owner;
    }
    public function updateHomeOwner(
        $description,
        $logo,
        $companyName,
        $userCancellationTime,
        $userRescheduleTime,
        $userCancellationRefundAmount,
        $userRescheduleRefundAmount
    ) {
        $homeOwner = $this::where('user_id', Auth::user()->id)->first();
        $homeOwner->description = $description;
        $homeOwner->company_name = $companyName;
        $homeOwner->logo = $logo;
        $homeOwner->user_cancellation_time = $userCancellationTime;
        $homeOwner->user_reschedule_time = $userRescheduleTime;
        $homeOwner->user_cancellation_refund_amount = $userCancellationRefundAmount;
        $homeOwner->user_reschedule_refund_amount = $userRescheduleRefundAmount;
        $homeOwner->save();
    }
}