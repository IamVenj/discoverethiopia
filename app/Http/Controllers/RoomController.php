<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\RoomImages;
use App\RoomType;
use App\HotelAmenity;
use App\Amenity;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    private $_room, $_roomType, $_roomImage, $_hotelAmenity, $_amenity;
    public function __construct()
    {
        $this->middleware(['auth', 'hotelAuth', 'wizard']);
        $this->_room = new Room();
        $this->_roomType = new RoomType();
        $this->_roomImage = new RoomImages();
        $this->_hotelAmenity = new HotelAmenity();
        $this->_amenity = new Amenity();
    }
    public function index()
    {
        return view('post.pages.hotel.rooms.index');
    }

    public function create()
    {
        return view('post.pages.hotel.rooms.create');
    }

    public function showImage($id)
    {
        return view('post.pages.hotel.rooms.showImage', compact('id'));
    }

    public function updateIndex($id)
    {
        return view('post.pages.hotel.rooms.edit', compact('id'));
    }

    public function getImages($id)
    {
        return $this->_roomImage->getRoomImages($id);
    }

    public function getData()
    {
        $roomtype = $this->_roomType::where('type', 'room')->get();
        $amenity = $this->_amenity::where('type', 'hotel')->get();
        $room = $this->_room::with(['images', 'amenities'])->latest()->paginate(10);
        return response()->json(['roomtype' => $roomtype, 'amenity' => $amenity, 'room' => $room], 200);
    }

    public function getRealData($id)
    {
        $roomtype = $this->_roomType::where('type', 'room')->get();
        $amenity = $this->_amenity::where('type', 'hotel')->get();
        $room = $this->_room::with(['images', 'amenities'])->find($id);
        return response()->json(['roomtype' => $roomtype, 'amenity' => $amenity, 'room' => $room], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'room_no' => 'required',
            'images' => 'required',
            'roomtype' => 'required',
            'amenity' => 'required',
            'basic_layout' => 'required',
            'smoking_status' => 'required',
            'price' => 'required',
            'bedroom' => 'required',
            'maximum_capacity' => 'required',
            'availability' => 'required',
            'square_feet' => 'required',
            'bathroom' => 'required',
            'bed_detail' => 'required',
        ]);
        $room = $this->_room->createRoom(
            $request->price,
            Auth::user()->id,
            $request->room_no,
            $request->roomtype,
            $request->availability,
            $request->maximum_capacity,
            $request->bedroom,
            $request->bed_detail,
            $request->square_feet,
            $request->bathroom,
            $request->basic_layout,
            $request->smoking_status
        );
        $this->_hotelAmenity->addAmenity($request->amenity, $room->id);
        $this->_roomImage->addImages($room->id, $request->images);
        return response()->json(['message' => 'Room is successfully updated!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'room_no' => 'required',
            'roomtype' => 'required',
            'amenity' => 'required',
            'basic_layout' => 'required',
            'smoking_status' => 'required',
            'price' => 'required',
            'bedroom' => 'required',
            'maximum_capacity' => 'required',
            'availability' => 'required',
            'square_feet' => 'required',
            'bathroom' => 'required',
            'bed_detail' => 'required',
        ]);

        $this->_room->updateRoom(
            $id,
            $request->price,
            Auth::user()->id,
            $request->room_no,
            $request->roomtype,
            $request->availability,
            $request->maximum_capacity,
            $request->bedroom,
            $request->bed_detail,
            $request->square_feet,
            $request->bathroom,
            $request->basic_layout,
            $request->smoking_status
        );
        $this->_hotelAmenity->updateAmenity($request->amenity, $id);

        return response()->json(['message' => 'Room is successfully updated!'], 200);
    }

    public function updateImage(Request $request, $id)
    {
        $request::validate([
            'image' => 'required'
        ]);
        $this->_roomImage->updateImage($id, $request->image);
        return response()->json(['message' => 'Image is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_room->destroyRoom($id);
        return response()->json(['message' => 'Room is successfully deleted!'], 200);
    }

    public function imageDestruction($id)
    {
        $this->_roomImage->destroyImage($id);
        return response()->json(['message' => 'Room image is successfully deleted!']);
    }
}