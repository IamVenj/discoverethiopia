<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agreement;

class CarAgreementController extends Controller
{
    private $_agreement;
    public function __construct()
    {
        $this->middleware(['auth', 'wizard', 'carAuth']);
        $this->_agreement = new Agreement();
    }

    public function index()
    {
        return view('post.pages.car.agreement.index');
    }

    public function store(Request $request)
    {
    }
}