<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    private $_review;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_review = new Review();
    }

    public function store(Request $request, $type)
    {
        $request->validate([
            'rating' => 'required',
            'review' => 'required'
        ]);
        $roomId = null;
        $propertyId = null;
        $carId = null;
        $tourId = null;
        $reviewee_id = $request->reviewee;

        if ($type == "hotel") {
            $roomId = $request->roomId;
        } elseif ($type == "home") {
            $propertyId = $request->propertyId;
        } elseif ($type == "car") {
            $carId = $request->carId;
        } elseif ($type == "tour") {
            $tourId = $request->tourId;
        }

        if ($this->_review->checkIfReviewWasPosted($reviewee_id, $roomId, $tourId, $carId, $propertyId) == 0) {
            $this->_review->createReview(Auth::user()->id, $reviewee_id, $request->review, $request->rating, $tourId, $carId, $roomId, $propertyId);
            return response()->json(['message' => 'Review is successfully posted!', 'exist' => 1], 200);
        } else {
            $myreview = $this->_review->getMyReviews($roomId, $tourId, $propertyId, $carId);
            $this->_review->updateReview($myreview->id, $request->review, $request->rating);
            return response()->json(['message' => 'Review is successfully updated', 'exist' => 0], 200);
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'rating' => 'required',
            'review' => 'required'
        ]);
        $this->_review->updateReview($id, $request->review, $request->rating);
        return response()->json(['message' => 'Review is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_review->destroyReview($id);
        return response()->json(['message' => 'Review is successfully deleted!'], 200);
    }
}