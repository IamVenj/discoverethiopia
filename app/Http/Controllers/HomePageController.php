<?php

namespace App\Http\Controllers;

use App\HomeOwner;
use App\Property;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomePageController extends Controller
{
    private $_owner, $_property, $_review;
    public function __construct()
    {
        $this->_property = new Property();
        $this->_owner = new HomeOwner();
        $this->_review = new Review();
    }
    public function index()
    {
        return view('pre-login.pages.vacationalHome.index');
    }

    public function getRating($roomId, $ownerId)
    {
        return $this->_review->calculateRatingForRooms($ownerId, $roomId);
    }

    public function indexVue()
    {
        $properties = $this->_property::with(['homeOwner.user.addresses', 'images', 'reviews', 'propertyType', 'homeOwner.agreement'])->latest()->get();
        return response()->json(['house' => $properties], 200);
    }

    public function show($id)
    {
        $property = $this->_property::with(['homeOwner.user.addresses', 'images'])->find($id);
        return view('pre-login.pages.vacationalHome.show', compact('property'));
    }

    public function showVue($id)
    {
        $calculatedRating = 0;
        $property = $this->_property::with(['homeOwner.user.addresses', 'homeOwner.agreement', 'images', 'amenities.amenities', 'reviews.reviewer', 'propertyType'])->find($id);
        !is_null(Auth::user()) ?
            $myReview = $this->_review->getMyReviews(null, null, $id, null) : $myReview = null;
        $state = Auth::user();
        if ($this->_review->totalCountOfReview($property->homeOwner->user->id) > 0) {
            $calculatedRating = $this->_review->calculateRating($property->homeOwner->user->id);
        }
        return response()->json(['property' => $property, 'myReview' => $myReview, 'state' => $state, 'calculatedRating' => $calculatedRating], 200);
    }
}