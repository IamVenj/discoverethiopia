<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminSettings;

class AdminAgreementController extends Controller
{
    private $_settings;
    public function __construct()
    {
        $this->middleware(['auth', 'adminAuth', 'wizard']);
        $this->_settings = new AdminSettings();
    }
    public function index()
    {
        return view('post.pages.admin.agreement.index');
    }
    public function getAgreements()
    {
        return $this->_settings::first();
    }
    public function update(Request $request)
    {
        $request->validate([
            'hotel' => 'required',
            'tour' => 'required',
            'car' => 'required',
            'customer' => 'required',
            'home' => 'required'
        ]);
        $this->_settings->updateSettings(
            $request->hotel,
            $request->tour,
            $request->car,
            $request->customer,
            $request->home
        );
        return response()->json(['message' => 'Agreement is successfully updated!'], 200);
    }
}
