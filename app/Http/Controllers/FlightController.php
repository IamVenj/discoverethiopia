<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FlightController extends Controller
{
    public function index()
    {
    	return view('pre-login.pages.flight.index');
    }

    public function show()
    {
    	return view('pre-login.pages.flight.show');
    }
}
