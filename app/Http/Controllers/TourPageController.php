<?php

namespace App\Http\Controllers;

use App\TourOwner;
use App\Tour;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TourPageController extends Controller
{
    private $_owner, $_tour, $_review;
    public function __construct()
    {
        $this->_owner = new TourOwner();
        $this->_tour = new Tour();
        $this->_review = new Review();
    }
    public function index()
    {
        return view('pre-login.pages.tour.index');
    }
    public function getRating($tourId, $ownerId)
    {
        return $this->_review->calculateRatingFortours($ownerId, $tourId);
    }
    public function indexVue()
    {
        $tourst = $this->_tour::with(['toursOwner.user.addresses', 'images', 'reviews', 'toursOwner.agreement'])->latest()->get();
        return response()->json(['tour' => $tourst], 200);
    }
    public function show($id)
    {
        $tour = $this->_tour::with(['toursOwner.user.addresses', 'images'])->find($id);
        return view('pre-login.pages.tour.show', compact('tour'));
    }
    public function showVue($id)
    {
        $calculatedRating = 0;
        $tour = $this->_tour::with(['toursOwner.user.addresses', 'toursOwner.agreement', 'images', 'reviews.reviewer'])->find($id);
        !is_null(Auth::user()) ?
            $myReview = $this->_review->getMyReviews($id, null, null, null) : $myReview = null;
        $state = Auth::user();
        if ($this->_review->totalCountOfReview($tour->toursOwner->user->id) > 0) {
            $calculatedRating = $this->_review->calculateRating($tour->toursOwner->user->id);
        }
        return response()->json(['tour' => $tour, 'myReview' => $myReview, 'state' => $state, 'calculatedRating' => $calculatedRating], 200);
    }
}