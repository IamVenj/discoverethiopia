<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotelBooking;
class HotelBookingController extends Controller
{
    private $_hotelBooking;
    public function __construct()
    {
        $this->middleware(['auth', 'hotelAuth', 'wizard']);
        $this->_hotelBooking = new HotelBooking();
    }
    public function index()
    {
        return view('post.pages.hotel.booking.index');
    }
    public function getBookedUsers()
    {
        return $this->_hotelBooking->getUsers();
    }
}
