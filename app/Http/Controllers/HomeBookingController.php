<?php

namespace App\Http\Controllers;

use App\HomeBooking;
use Illuminate\Http\Request;

class HomeBookingController extends Controller
{
    private $_homeBooking;
    public function __construct()
    {
        $this->middleware(['auth', 'homeAuth', 'wizard']);
        $this->_homeBooking = new HomeBooking();
    }
    public function index()
    {
        return view('post.pages.home.booking.index');
    }
    public function getBookedUsers()
    {
        return $this->_homeBooking->getUsers();
    }
}