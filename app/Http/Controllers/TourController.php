<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\TourImage;
use App\TourLocation;
use Illuminate\Support\Facades\Auth;

class TourController extends Controller
{
    private $_tour, $_tourImage, $_tourLocation;

    public function __construct()
    {
        $this->middleware(['auth', 'tourAuth', 'wizard']);
        $this->_tour = new Tour();
        $this->_tourImage = new tourImage();
        $this->_tourLocation = new TourLocation();
    }
    public function index()
    {
        return view('pre-login.pages.tour.index');
    }

    public function getTour()
    {
        return view('post.pages.tour.tours.index');
    }

    public function getData()
    {

        $tours = $this->_tour::with(['images'])->latest()->paginate(10);
        return response()->json(['tours' => $tours], 200);
    }
    public function updateIndex($id)
    {
        return view('post.pages.tour.tours.edit', compact('id'));
    }
    public function getRealData($id)
    {
        $tours = $this->_tour::with(['images'])->latest()->paginate(10);
        return response()->json(['tour' => $tours], 200);
    }

    public function showImage()
    {
        return view('post.pages.tour.tours.showImage');
    }

    public function getImages()
    {
        return $this->_tourImage::latest()->get();
    }

    public function create()
    {
        return view('post.pages.tour.tours.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'tour_date' => 'required',
            'departs_from' => 'required',
            'arrives_at' => 'required',
            'price_per_person' => 'required',
            'images' => 'required',
        ]);


        $departsFrom = $request->departs_from;
        $arrivesAt = $request->arrives_at;
        for ($i = 0; $i < count($departsFrom); $i++) {

            $departsFromlat = $departsFrom[$i]['geometry']['location']['lat'];
            $departsFromlng = $departsFrom[$i]['geometry']['location']['lng'];
            $departsFromAddress = $departsFrom[$i]['name'] . ", " . $departsFrom[$i]['formatted_address'];

            $arrivesAtlat = $arrivesAt[$i]['geometry']['location']['lat'];
            $arrivesAtlng = $arrivesAt[$i]['geometry']['location']['lng'];
            $arrivesAtAddress = $arrivesAt[$i]['name'] . ", " . $arrivesAt[$i]['formatted_address'];

            $tourLocation = $this->_tourLocation->createTourLocation(
                $departsFromlat,
                $departsFromlng,
                $arrivesAtlat,
                $arrivesAtlng,
            );

            $tour = $this->_tour->createTour(
                $request->name,
                $request->description,
                $request->tour_date,
                $departsFromAddress,
                $arrivesAtAddress,
                $request->price_per_person,
                $tourLocation->id,
                Auth::user()->id,
            );
        }



        $this->_tourImage->addImages($tour->id, $request->images);
        return response()->json(['message' => 'Tour is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'tour_date' => 'required',
            'departs_from' => 'required',
            'arrives_at' => 'required',
            'price_per_person' => 'required',
            'images' => 'required',
        ]);
        $name = $request->name;
        $description = $request->description;
        $tourDate = $request->tourDate;
        $departsFrom = $request->departsFrom;
        $tourLocationId = $request->tourLocationId;
        $arrivesAt = $request->arrivesAt;
        $price = $request->price;
        $ownerId = Auth::user()->id;
        $this->_tour->updateTour(
            $id,
            $name,
            $description,
            $tourDate,
            $departsFrom,
            $tourLocationId,
            $arrivesAt,
            $price,
            $ownerId
        );
        return response()->json(['message' => 'Tour is successfully updated!'], 200);
    }
    public function destroy($id)
    {
        $this->tour->destroyTour($id);
        return response()->json(['message' => 'Tour is successfully deleted!'], 200);
    }
    public function imageDestruction($id)
    {
        $this->_tourImage->destroyImage($id);
        return response()->json(['message' => 'Tour image is successfully deleted!']);
    }
}