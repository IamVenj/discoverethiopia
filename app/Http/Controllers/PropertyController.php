<?php

namespace App\Http\Controllers;

use App\Amenity;
use App\HomeAmenity;
use App\Property;
use App\PropertyImage;
use App\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    private $_amenity, $_homeAmenity, $_propertyImage, $_roomType, $_property;
    public function __construct()
    {
        $this->middleware(['auth', 'homeAuth', 'wizard']);
        $this->_amenity = new Amenity();
        $this->_homeAmenity = new HomeAmenity();
        $this->_propertyImage = new PropertyImage();
        $this->_roomType = new RoomType();
        $this->_property = new Property();
    }
    public function index()
    {
        return view('post.pages.home.property.index');
    }

    public function create()
    {
        return view('post.pages.home.property.create');
    }

    public function getImages($id)
    {
        return $this->_propertyImage->getPropertyImages($id);
    }

    public function showImage($id)
    {
        return view('post.pages.home.property.showImage', compact('id'));
    }

    public function getData()
    {
        $roomtype = $this->_roomType::where('type', 'property')->get();
        $amenity = $this->_amenity::where('type', 'home')->get();
        $properties = $this->_property::with(['images', 'amenities'])->latest()->paginate(10);
        return response()->json(['roomtype' => $roomtype, 'amenity' => $amenity, 'properties' => $properties], 200);
    }

    public function updateIndex($id)
    {
        return view('post.pages.home.property.edit', compact('id'));
    }

    public function getRealData($id)
    {
        $roomtype = $this->_roomType::where('type', 'property')->get();
        $amenity = $this->_amenity::where('type', 'home')->get();
        $property = $this->_property::with(['images', 'amenities'])->find($id);
        return response()->json(['roomtype' => $roomtype, 'amenity' => $amenity, 'property' => $property], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'images' => 'required',
            'propertyType' => 'required',
            'address' => 'required',
            'amenity' => 'required',
            'basic_layout' => 'required',
            'price' => 'required',
            'bedroom' => 'required',
            'maximum_capacity' => 'required',
            'availability' => 'required',
            'square_feet' => 'required',
            'bathroom' => 'required',
        ]);
        $name = $request->name;
        $description = $request->description;
        $price = $request->price;
        $owner = Auth::user()->id;
        $address_array = $request->address;
        for ($i = 0; $i < count($address_array); $i++) {
            $lat = $address_array[$i]['geometry']['location']['lat'];
            $lng = $address_array[$i]['geometry']['location']['lng'];
            $addressName = $address_array[$i]['name'] . ", " . $address_array[$i]['formatted_address'];
        }
        $availability = $request->availability;
        $maximum_capacity = $request->maximum_capacity;
        $bedroom = $request->bedroom;
        $bed_detail = $request->bed_detail;
        $square_feet = $request->square_feet;
        $bathroom = $request->bathroom;
        $basic_layout = $request->basic_layout;
        $propertyType = $request->propertyType;
        $property = $this->_property->createProperty(
            $name,
            $description,
            $price,
            $owner,
            $addressName,
            $lat,
            $lng,
            $availability,
            $maximum_capacity,
            $bedroom,
            $bed_detail,
            $square_feet,
            $bathroom,
            $basic_layout,
            $propertyType
        );
        $this->_homeAmenity->addAmenity($request->amenity, $property->id);
        $this->_propertyImage->addImages($property->id, $request->images);
        return response()->json(['message' => 'Property is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'propertyType' => 'required',
            'amenity' => 'required',
            'basic_layout' => 'required',
            'price' => 'required',
            'bedroom' => 'required',
            'maximum_capacity' => 'required',
            'availability' => 'required',
            'square_feet' => 'required',
            'bathroom' => 'required',
            'bed_detail' => 'required',
        ]);
        $name = $request->name;
        $description = $request->description;
        $price = $request->price;
        $owner = Auth::user()->id;
        $availability = $request->availability;
        $maximum_capacity = $request->maximum_capacity;
        $bedroom = $request->bedroom;
        $bed_detail = $request->bed_detail;
        $square_feet = $request->square_feet;
        $bathroom = $request->bathroom;
        $basic_layout = $request->basic_layout;
        $propertyType = $request->propertyType;
        $this->_property->updateProperty(
            $id,
            $name,
            $description,
            $price,
            $owner,
            $availability,
            $maximum_capacity,
            $bedroom,
            $bed_detail,
            $square_feet,
            $bathroom,
            $basic_layout,
            $propertyType
        );
        $this->_homeAmenity->updateAmenity($request->amenity, $id);
        return response()->json(['message' => 'Property is successfully updated!'], 200);
    }

    public function updateImage(Request $request, $id)
    {
        $request::validate([
            'image' => 'required'
        ]);
        $this->_propertyImage->updateImage($id, $request->image);
        return response()->json(['message' => 'Image is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_property->destroyProperty($id);
        return response()->json(['message' => 'Property is successfully deleted!'], 200);
    }

    public function imageDestruction($id)
    {
        $this->_propertyImage->destroyImage($id);
        return response()->json(['message' => 'Property image is successfully deleted!']);
    }

    public function updateAddress()
    {
    }
}