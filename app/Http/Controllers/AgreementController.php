<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\UserAgreement;
use Illuminate\Support\Facades\Auth;

class AgreementController extends Controller
{
    private $_agreement;
    public function __construct()
    {
        $this->_agreement = new UserAgreement();
    }
    public function agree(Request $request)
    {
        $request->validate([
            'agreement_id' => 'required'
        ]);
        $this->_agreement->addAgreement(Auth::user()->id, $request->agreement_id);
        return response()->json(['status' => 'success'], 200);
    }

    public function checkAgreement($agreement_id)
    {
        if (!is_null(Auth::user())) {
            $agreementCount = $this->_agreement->checkIfCustomerHasAgreed(Auth::user()->id, $agreement_id);
            return $agreementCount > 0 ?
                response()->json(['exist' => true, 'status' => 'auth'], 200) : response()->json(['status' => 'auth', 'exist' => false], 200);
        } else {
            return response()->json(['status' => 'notAuth']);
        }
    }
}