<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use App\carImage;
use App\Driver;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    private $_car, $_carImage, $_driver;
    public function __construct()
    {
        $this->middleware(['auth', 'carAuth', 'wizard']);
        $this->_car = new Car();
        $this->_carImage = new carImage();
        $this->_driver = new Driver();
    }

    public function index()
    {
        return view('pre-login.pages.cars.index');
    }

    public function show()
    {
        return view('pre-login.pages.cars.show');
    }
    public function showImage()
    {
        return view('post.pages.car.cars.showImage');
    }
    public function getImages()
    {
        return $this->_carImage::latest()->get();
    }
    public function getCar()
    {

        return view('post.pages.car.cars.index');
    }
    public function getData()
    {
        $drivers = $this->_driver::all();
        $cars = $this->_car::with(['images'])->latest()->paginate(10);
        return response()->json(['cars' => $cars, 'drivers' => $drivers], 200);
    }
    public function getCars()
    {
        return $this->car::latest()->paginate(10);
    }
    public function create()
    {
        return view('post.pages.car.cars.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'model' => 'required',
            'images' => 'required',
            'description' => 'required',
            'price' => 'required',
            'availability' => 'required',
            'driver_id' => 'required',
        ]);
        $car = $this->_car->createCar(
            $request->name,
            $request->model,
            $request->description,
            Auth::user()->id,
            $request->driver,
            $request->price,
            $request->availability,
            $request->driver_id,
        );
        $this->_carImage->addImages($car->id, $request->images);
        return response()->json(['message' => 'Car is successfully created!'], 200);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'model' => 'required',
            'description' => 'required',
            'driver_id' => 'required',
            'price' => 'required',
            'availability' => 'required',
        ]);
        $this->car->updateCar($request->name, $request->model, $request->description, $request->price, $request->availablity);
        return response()->json(['message' => 'Car is successfully created!'], 200);
    }
    public function destroy($id)
    {
        $this->car->destroyCar($id);
        return response()->json(['message' => 'car is successfully deleted!'], 200);
    }
    public function imageDestruction($id)
    {
        $this->_carImage->destroyImage($id);
        return response()->json(['message' => 'Car image is successfully deleted!']);
    }
}