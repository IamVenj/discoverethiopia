<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use App\CarBooking;
use App\HomeBooking;
use App\FlightBooking;
use App\HotelBooking;
use App\Property;
use App\Room;
use App\Tour;
use App\TourBooking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class BookingController extends Controller
{
    private $_carBooking, $_homeBooking, $_flightBooking, $_hotelBooking, $_tourBooking;
    private $_car, $_property, $_room, $_tour;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_carBooking = new CarBooking();
        $this->_homeBooking = new HomeBooking();
        $this->_flightBooking = new FlightBooking();
        $this->_hotelBooking = new HotelBooking();
        $this->_tourBooking = new TourBooking();
        $this->_car = new Car();
        $this->_property = new Property();
        $this->_tour = new Tour();
        $this->_room = new Room();
    }
    public function index($id, $type)
    {
        return view('pre-login.pages.booking.index', compact('id', 'type'));
    }

    public function recoupData($id, $type)
    {
        $recoupedData = null;
        $bookingData = null;
        if ($type == "room") {
            $recoupedData = $this->_room->recoupDataById($id);
            $bookingData = $this->_hotelBooking->getRoomBooks($id);
        } elseif ($type == "property") {
            $recoupedData = $this->_property->recoupDataById($id);
            $bookingData = $this->_homeBooking->getPropertyBooks($id);
        } elseif ($type == "tour") {
            $recoupedData = $this->_tour->recoupDataById($id);
            $bookingData = $this->_tourBooking->getTourBooks($id);
        } elseif ($type == "car") {
            $recoupedData = $this->_car->recoupDataById($id);
            $bookingData = $this->_carBooking->getCarBooks($id);
        }
        return response()->json(['data' => $recoupedData, 'books' => $bookingData, 'userFact' => Auth::user()]);
    }

    public function store(Request $request, $id, $type)
    {
        $request->validate([]);
        $contents = null;
        $booking = null;

        if ($type == "room" || $type == "property") {
            $checkin = Carbon::createFromDate($request->checkin);
            $checkout = Carbon::createFromDate($request->checkout);
            $contents = 'Type ' . $type . ' | name: ' . $request->data['name'] . ' | Address: ' . $request->data['address'];
        } elseif ($type == "flight") {
            $departingDate = Carbon::createFromDate($request->departingDate);
            $arrivalDate = Carbon::createFromDate($request->arrivalDate);
            $contents = 'Type ' . $type . ' | name: ' . $request->data['name'] . ' | Address: ' . $request->data['address'];
        } elseif ($type == "tour") {
            $contents = 'Type ' . $type . ' | name: ' . $request->data['name'] . ' | Address: ' . $request->data['address'];
        } elseif ($type == "car") {
            $checkin = Carbon::createFromDate($request->checkin);
            $checkout = Carbon::createFromDate($request->checkout);
            $contents = 'Type ' . $type . ' | name: ' . $request->data['name'] . ' | Address: ' . $request->data['address'];
        }
        try {
            if ($type == "room") {
                if ($this->_hotelBooking->identifyBookedPropertyInTimeInterval($checkin, $checkout, $id) == 0) {
                    $charge = Stripe::charges()->create([
                        'amount' => $request->totalPrice,
                        'currency' => 'USD',
                        'source' => $request->stkn,
                        'receipt_email' => $request->email,
                        'metadata' => [
                            'contents' => $contents,
                        ]
                    ]);

                    $booking = $this->_hotelBooking->bookRoom(
                        Auth::user()->id,
                        $id,
                        $checkin,
                        $checkout,
                        $request->adults,
                        $request->children,
                        $request->infant,
                        $request->extra_info,
                        $charge['id']
                    );
                }
            }
            //  elseif ($type == "flight") {
            //     // if ($this->_room->checkAvailability($id) == 'available')
            //     $charge = Stripe::charges()->create([
            //         'amount' => $request->totalPrice,
            //         'currency' => 'USD',
            //         'source' => $request->stkn,
            //         'receipt_email' => $request->email,
            //         'metadata' => [
            //             'contents' => $contents,
            //         ]
            //     ]);
            //     $booking = $this->_flightBooking->bookFlight(
            //         $request->flightname,
            //         $request->flyingfrom,
            //         $departingDate,
            //         $arrivalDate,
            //         $request->flightType,
            //         $request->adults,
            //         $request->children,
            //         $request->infant,
            //         $charge['id']
            //     );
            // }
            elseif ($type == "property") {
                if ($this->_homeBooking->identifyBookedPropertyInTimeInterval($checkin, $checkout, $id) == 0) {
                    $charge = Stripe::charges()->create([
                        'amount' => $request->totalPrice,
                        'currency' => 'USD',
                        'source' => $request->stkn,
                        'receipt_email' => $request->email,
                        'metadata' => [
                            'contents' => $contents,
                        ]
                    ]);
                    $booking = $this->_homeBooking->bookProperty(
                        Auth::user()->id,
                        $id,
                        $checkin,
                        $checkout,
                        $request->adults,
                        $request->children,
                        $request->infant,
                        $charge['id']
                    );
                }
            } elseif ($type == "tour") {
                if ($this->_tour->checkAvailability($id) == 'available') {
                    $charge = Stripe::charges()->create([
                        'amount' => $request->totalPrice,
                        'currency' => 'USD',
                        'source' => $request->stkn,
                        'receipt_email' => $request->email,
                        'metadata' => [
                            'contents' => $contents,
                        ]
                    ]);
                    $booking = $this->_tourBooking->bookTour(
                        $id,
                        $request->extraInfo,
                        $charge['id']
                    );
                }
            } elseif ($type == "car") {
                if ($this->_carBooking->identifyBookedPropertyInTimeInterval($checkin, $checkout, $id) == 0) {
                    $charge = Stripe::charges()->create([
                        'amount' => $request->totalPrice,
                        'currency' => 'USD',
                        'source' => $request->stkn,
                        'receipt_email' => $request->email,
                        'metadata' => [
                            'contents' => $contents,
                        ]
                    ]);
                    $booking = $this->_carBooking->bookCar(
                        Auth::user()->id,
                        $id,
                        $checkin,
                        $checkout,
                        $request->extra_info,
                        $charge['id']
                    );
                }
            }
        } catch (CardErrorException $e) {
            return response()->json(['status' => 0, 'message' => 'Error! ' . $e->getMessage()], 200);
        }
        return $booking != null ? response()->json(['status' => 1, 'message' => 'You are successfully booked!'], 200) :
            response()->json(['status' => 0, 'message' => 'Sorry you cannot book! It\'s unavailable']);
    }
}