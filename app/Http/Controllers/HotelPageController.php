<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\HotelOwner;
use App\Review;
use Illuminate\Support\Facades\Auth;

class HotelPageController extends Controller
{
    private $_owner, $_room, $_review;
    public function __construct()
    {
        $this->_room = new Room();
        $this->_owner = new HotelOwner();
        $this->_review = new Review();
    }
    public function index()
    {
        return view('pre-login.pages.hotel.index');
    }

    public function show($id)
    {
        $owner = $this->_owner::with(['user.addresses'])->where('user_id', $id)->first();
        return view('pre-login.pages.hotel.show', compact('owner'));
    }

    public function showVue($id)
    {
        $calculatedRating = 0;
        $price = $this->_room::min('price_per_night');
        $owner = $this->_owner::with(['user.addresses', 'rooms.images', 'rooms.roomType', 'agreement', 'rooms.reviews'])->where('user_id', $id)->first();
        if ($this->_review->totalCountOfReview($id) > 0) {
            $calculatedRating = $this->_review->calculateRating($id);
        }
        $totalCountOfReview = $this->_review->totalCountOfReview($id);
        return response()->json(['owner' => $owner, 'price' => $price, 'calculatedRating' => $calculatedRating, 'totalCountOfReview' => $totalCountOfReview], 200);
    }

    public function getRating($roomId, $ownerId)
    {
        return $this->_review->calculateRatingForRooms($ownerId, $roomId);
    }

    public function indexVue()
    {
        $price = $this->_room::min('price_per_night');
        $owner = $this->_owner::with(['user.addresses', 'rooms'])->latest()->get();
        return response()->json(['hotel' => $owner, 'price' => $price], 200);
    }

    public function showRooms($id)
    {
        $room = $this->_room::with(['hotelOwner.user.addresses', 'images'])->find($id);
        return view('pre-login.pages.hotel.showRoom', compact('room'));
    }

    public function showRoomsVue($id)
    {
        $calculatedRating = 0;
        $room = $this->_room::with(['hotelOwner.user.addresses', 'hotelOwner.agreement', 'images', 'amenities.amenities', 'reviews.reviewer'])->find($id);
        !is_null(Auth::user()) ?
            $myReview = $this->_review->getMyReviews($id, null, null, null) : $myReview = null;
        $state = Auth::user();

        if ($this->_review->totalCountOfReview($room->hotelOwner->user->id) > 0) {
            $calculatedRating = $this->_review->calculateRating($room->hotelOwner->user->id);
        }
        return response()->json(['room' => $room, 'myReview' => $myReview, 'state' => $state, 'calculatedRating' => $calculatedRating], 200);
    }
}