<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TourAgreementController extends Controller
{
    public function index()
    {
        return view('post.pages.tour.agreement.index');
    }
}