<?php

namespace App\Http\Controllers;

use App\TourBooking;
use Illuminate\Http\Request;

class TourBookingController extends Controller
{
    private $_tourBooking;
    public function __construct()
    {
        $this->middleware(['auth', 'tourAuth', 'wizard']);
        $this->_tourBooking = new TourBooking();
    }
    public function index()
    {
        return view('post.pages.tour.booking.index');
    }
    public function getTourUsers()
    {
        return $this->_tourBooking->getUsers();
    }
}