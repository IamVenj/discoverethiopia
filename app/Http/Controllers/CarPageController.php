<?php

namespace App\Http\Controllers;

use App\Car;
use App\CarCompanyOwner;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CarPageController extends Controller
{
    private $_owner, $_car, $_review;
    public function __construct()
    {
        $this->_car = new Car();
        $this->_owner = new CarCompanyOwner();
        $this->_review = new Review();
    }

    public function index()
    {
        return view('pre-login.pages.cars.index');
    }

    public function show($id)
    {
        $car = $this->_car::with(['carsOwner.user.addresses', 'images'])->find($id);
        return view('pre-login.pages.cars.show', compact('car'));
    }

    public function showVue($id)
    {
        $calculatedRating = 0;
        $car = $this->_car::with(['carsOwner.user.addresses', 'carsOwner.agreement', 'images', 'reviews.reviewer'])->find($id);
        !is_null(Auth::user()) ?
            $myReview = $this->_review->getMyReviews($id, null, null, null) : $myReview = null;
        $state = Auth::user();
        if ($this->_review->totalCountOfReview($car->carsOwner->user->id) > 0) {
            $calculatedRating = $this->_review->calculateRating($car->carsOwner->user->id);
        }
        return response()->json(['car' => $car, 'myReview' => $myReview, 'state' => $state, 'calculatedRating' => $calculatedRating], 200);
    }

    public function getRating($carId, $ownerId)
    {
        return $this->_review->calculateRatingForRooms($ownerId, $carId);
    }

    public function indexVue()
    {
        $carst = $this->_car::with(['carsOwner.user.addresses', 'images', 'reviews', 'carsOwner.agreement'])->latest()->get();
        return response()->json(['car' => $carst], 200);
    }
}