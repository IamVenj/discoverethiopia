<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    private $_user;
    public function __construct()
    {
        $this->middleware(['auth', 'adminAuth', 'wizard']);
        $this->_user = new User();
    }
    public function index()
    {
        return view('post.pages.admin.users.index');
    }

    public function show($userType)
    {
        return view('post.pages.admin.users.show', compact('userType'));
    }
    public function getUsers($userType)
    {
        $users = null;
        if ($userType == "customers") {
            $users = $this->_user::where('role', 6)->latest()->paginate();
        }
        if ($userType == "tour") {
            $users = $this->_user::with(['tourOwners'])->where('role', 5)
                ->whereHas('tourOwners', function ($query) {
                    $query->whereNotNull('company_name');
                })->latest()->paginate(10);
        }
        if ($userType == "hotel") {
            $users = $this->_user::with(['hotelOwners'])->where('role', 2)
                ->whereHas('hotelOwners', function ($query) {
                    $query->whereNotNull('company_name');
                })->latest()->paginate(10);
        }
        if ($userType == "home") {
            $users = $this->_user::with(['homeOwners'])->where('role', 3)
                ->whereHas('homeOwners', function ($query) {
                    $query->whereNotNull('company_name');
                })->latest()->paginate(10);
        }
        if ($userType == "car") {
            $users = $this->_user::with(['carCompanyOwners'])->where('role', 4)
                ->whereHas('carCompanyOwners', function ($query) {
                    $query->whereNotNull('company_name');
                })->latest()->paginate(10);
        }
        return $users;
    }
}
