<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarBooking;

class CarBookingController extends Controller
{
    private $_carBooking;
    public function __construct()
    {
        $this->middleware(['auth', 'carAuth', 'wizard']);
        $this->_carBooking = new CarBooking();
    }
    public function index()
    {
        return view('post.pages.car.booking.index');
    }
    public function getCarUsers()
    {
        return $this->_carBooking->getUsers();
    }
}