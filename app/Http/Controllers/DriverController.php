<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;


class DriverController extends Controller
{
    private $_driver;
    public function __construct()
    {
        $this->middleware(['auth', 'wizard', 'carAuth']);
        $this->_driver = new Driver();
    }

    public function index()
    {
        return view('post.pages.car.driver.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);
        $this->_driver->createDriver($request->name, $request->description);
        return response()->json(['message' => 'Driver is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required']);
        $this->_driver->updateDriver($request->name, $request->description, $id);
        return response()->json(['message' => 'Driver is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_driver->destroyDriver($id);
        return response()->json(['message' => 'Driver is successfully deleted!'], 200);
    }

    public function getDrivers()
    {
        return $this->_driver::all();
    }
}