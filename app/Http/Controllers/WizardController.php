<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use App\Agreement;
use App\CarCompanyOwner;
use App\HomeOwner;
use App\HotelOwner;
use App\TourOwner;
use Illuminate\Support\Facades\Auth;

class WizardController extends Controller
{
    private $_user, $_address, $_homeOwner, $_hotelOwner, $_tourOwner, $_carOwner, $_agreement;

    public function __construct()
    {
        $this->middleware(['auth', 'authenticatedWizard']);

        $this->_user = new User();
        $this->_address = new Address();
        $this->_homeOwner = new HomeOwner();
        $this->_hotelOwner = new HotelOwner();
        $this->_tourOwner = new TourOwner();
        $this->_carOwner = new CarCompanyOwner();
        $this->_agreement = new Agreement();
    }

    public function index()
    {
        return view('pre-login.pages.auth.wizard');
    }

    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'logo' => 'required',
            'company' => 'required',
            'description' => 'required',
            'phone1' => 'required',
            'agreement' => 'required',
            'userAgreement' => 'required'
        ]);

        $photo = $request->photo;
        $logo = $request->logo;
        $company = $request->company;
        $description = $request->description;
        $phone1 = $request->phone1;
        $phone2 = $request->phone2;
        $phone3 = $request->phone3;
        $address = $request->address;
        $role = Auth::user()->role;

        $user = $this->_user->updateUserWizard($photo, $phone1, $phone2, $phone3);

        if ($role == 2) {
            $this->_hotelOwner->createHotelOwner(
                $user->id,
                $description,
                $logo,
                $company,
                $request->user_cancellation_time,
                $request->user_reschedule_time,
                $request->user_cancellation_refund_amount,
                $request->user_reschedule_refund_amount
            );
        }
        if ($role == 3) {
            $this->_homeOwner->createHomeOwner(
                $user->id,
                $description,
                $logo,
                $company,
                $request->user_cancellation_time,
                $request->user_reschedule_time,
                $request->user_cancellation_refund_amount,
                $request->user_reschedule_refund_amount
            );
        }
        if ($role == 4) {
            $this->_carOwner->createCarOwner(
                $user->id,
                $description,
                $logo,
                $company,
                $request->user_cancellation_time,
                $request->user_reschedule_time,
                $request->user_cancellation_refund_amount,
                $request->user_reschedule_refund_amount
            );
        }
        if ($role == 5) {
            $this->_tourOwner->createTourOwner(
                $user->id,
                $description,
                $logo,
                $company,
                $request->user_cancellation_time,
                $request->user_reschedule_time,
                $request->user_cancellation_refund_amount,
                $request->user_reschedule_refund_amount
            );
        }

        $this->_address->createAddress($address);
        $this->_agreement->createAgreement(Auth::user()->id, $request->userAgreement);
        return response()->json(['status' => 'success'], 200);
    }
}