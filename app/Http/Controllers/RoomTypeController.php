<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomType;

class RoomTypeController extends Controller
{
    private $_roomType;
    public function __construct()
    {
        $this->middleware(['auth', 'adminAuth', 'wizard']);
        $this->_roomType = new RoomType();
    }
    public function index()
    {
        return view('post.pages.admin.roomtype.index');
    }


    public function getRoomtype()
    {
        return $this->_roomType::latest()->paginate(10);
    }


    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'name' => 'required',
            'type' => 'required'
        ]);

        $this->_roomType->createRoomType($request->name, $request->description, $request->type);
        return response()->json(['message' => 'Roomtype is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required',
            'name' => 'required',
            'type' => 'required'
        ]);
        $this->_roomType->updateRoomType($request->name, $request->description, $request->type, $id);
        return response()->json(['message' => 'Roomtype is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_roomType->destroyRoomType($id);
        return response()->json(['message' => 'Roomtype is successfully deleted!'], 200);
    }
}