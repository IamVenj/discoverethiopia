<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agreement;
use Illuminate\Support\Facades\Auth;

class HotelAgreementController extends Controller
{
    private $_agreement;
    public function __construct()
    {
        $this->middleware(['auth', 'hotelAuth', 'wizard']);
        $this->_agreement = new Agreement();
    }
    public function index()
    {
        return view('post.pages.hotel.agreement.index');
    }
    public function retrieveData()
    {
        return $this->_agreement::where('user_id', Auth::user()->id)->first();
    }

    public function update(Request $request, $id)
    {
        $request->validate(['agreement' => 'required']);
        $this->_agreement->updateAgreement($request->agreement, $id);
        return response()->json(['message' => 'Agreement is successfully updated!'], 200);
    }
}