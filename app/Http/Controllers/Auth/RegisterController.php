<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\HomeOwner;
use App\HotelOwner;
use App\TourOwner;
use App\CarCompanyOwner;

class RegisterController extends Controller
{
    private $_user, $_homeOwner, $_hotelOwner, $_tourOwner, $_carOwner;
    public function __construct()
    {
        $this->middleware('guest');
        $this->_user = new User();
        $this->_homeOwner = new HomeOwner();
        $this->_hotelOwner = new HotelOwner();
        $this->_tourOwner = new TourOwner();
        $this->_carOwner = new CarCompanyOwner();
    }

    public function storeCustomer(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required',
            'nationality' => 'required'
        ]);

        $this->_user->registerCH(
            $request->firstname,
            $request->lastname,
            Hash::make($request->password),
            $request->email,
            $request->phone,
            $request->nationality,
            6
        );
        if (Auth::attempt(request(['email', 'password']))) {
            return response()->json(["message" => "You are successfully registered!", "role" => 6], 200);
        }
    }

    public function storeHotel(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required|email|max:255|unique:users',
        ]);

        $this->_user->registerOwners(
            $request->email,
            Hash::make($request->password),
            2
        );
        if (Auth::attempt(request(['email', 'password']))) {
            return response()->json(["message" => "You are successfully registered!", "role" => 2], 200);
        }
    }

    public function storeHome(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required',
            'nationality' => 'required'
        ]);

        $this->_user->registerCH(
            $request->firstname,
            $request->lastname,
            Hash::make($request->password),
            $request->email,
            $request->phone,
            $request->nationality,
            3
        );
        if (Auth::attempt(request(['email', 'password']))) {
            return response()->json(["message" => "You are successfully registered!", "role" => 3], 200);
        }
    }

    public function storeCar(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required|email|max:255|unique:users',
        ]);

        $this->_user->registerOwners(
            $request->email,
            Hash::make($request->password),
            4
        );
        if (Auth::attempt(request(['email', 'password']))) {
            return response()->json(["message" => "You are successfully registered!", "role" => 4], 200);
        }
    }

    public function storeTour(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'email' => 'required|email|max:255|unique:users',
        ]);

        $this->_user->registerOwners(
            $request->email,
            Hash::make($request->password),
            5
        );
        if (Auth::attempt(request(['email', 'password']))) {
            return response()->json(["message" => "You are successfully registered!", "role" => 5], 200);
        }
    }
}