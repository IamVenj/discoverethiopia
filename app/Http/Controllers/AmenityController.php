<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Amenity;

class AmenityController extends Controller
{
    private $_amenity;
    public function __construct()
    {
        $this->middleware(['auth', 'adminAuth', 'wizard']);
        $this->_amenity = new Amenity();
    }
    public function index()
    {
        return view('post.pages.admin.amenity.index');
    }
    public function store(Request $request)
    {
        $request->validate([
            'amenityName' => 'required',
            'amenityType' => 'required'
        ]);

        $this->_amenity->createAmenity($request->amenityName, $request->amenityType);
        return response()->json(['message' => 'Amenity is successfully created!']);
    }

    public function getAmenities()
    {
        return $this->_amenity::latest()->paginate(10);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'amenityName' => 'required',
            'amenityType' => 'required'
        ]);

        $this->_amenity->updateAmenity($id, $request->amenityName, $request->amenityType);
        return response()->json(['message' => 'Amenity is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_amenity->destroyAmenity($id);
        return response()->json(['message' => 'Amenity is successfully deleted!'], 200);
    }
}