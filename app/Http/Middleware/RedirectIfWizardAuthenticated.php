<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfWizardAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null(Auth::user())) {
            if (Auth::user()->role == 2) {
                if (!is_null(Auth::user()->phone_number) && Auth::user()->hotelOwners()->count() > 0) {
                    return redirect('/dashboard');
                } else {

                    return $next($request);
                }
            }
            if (Auth::user()->role == 3) {
                if (Auth::user()->homeOwners()->count() > 0) {
                    return redirect('/dashboard');
                } else {
                    return $next($request);
                }
            }
            if (Auth::user()->role == 4) {
                if (!is_null(Auth::user()->phone_number) && Auth::user()->carCompanyOwners()->count() > 0) {
                    return redirect('/dashboard');
                } else {
                    return $next($request);
                }
            }
            if (Auth::user()->role == 5) {
                if (!is_null(Auth::user()->phone_number) && Auth::user()->homeOwners()->count() > 0) {
                    return redirect('/dashboard');
                } else {
                    return $next($request);
                }
            }
        }
        return $next($request);
    }
}