<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\CarCompanyOwner;
use App\Driver;
use App\CarImage;
use App\CarBooking;
use App\Review;

class Car extends Model
{
    protected $fillable = ['name', 'model', 'description', 'price', 'car_owners_id', 'driver_id', 'availability'];
    protected $hidden = ['car_owners_id', 'driver_id'];

    public function carOwner()
    {
        return $this->hasOne(CarCompanyOwner::class, 'user_id', 'car_owners_id');
    }

    public function carsOwner()
    {
        return $this->hasOne(CarCompanyOwner::class, 'user_id', 'car_owners_id');
    }

    public function driver()
    {
        return $this->hasOne(Driver::class, 'id', 'driver_id');
    }

    public function images()
    {
        return $this->hasMany(CarImage::class);
    }

    public function bookings()
    {
        return $this->hasMany(CarBooking::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'car_id', 'id');
    }

    public function recoupDataById($id)
    {
        return $this::with(['carOwner.user', 'driver'])->find($id);
    }

    public function setAvailability($id, $availability)
    {
        $car = $this::find($id);
        $car->availability = $availability;
        $car->save();
    }

    public function checkAvailability($id)
    {
        return $this::find($id)->availability;
    }

    public function createCar($name, $model, $description, $carOwner, $driver, $price, $availability)
    {
        $car = $this::create([
            'name' => $name,
            'model' => $model,
            'description' => $description,
            'car_owners_id' => $carOwner,
            'driver_id' => $driver,
            'price' => $price,
            'availability' => $availability
        ]);
        return $car;
    }

    public function updateCar($id, $name, $model, $description, $carOwner, $driver)
    {
        $car = $this::find($id);
        $car->name = $name;
        $car->model = $model;
        $car->description = $description;
        $car->carOwner = $carOwner;
        $car->driver = $driver;
        $car->save();
    }

    public function destroyCar($id)
    {
        $this::find($id)->delete();
    }
}