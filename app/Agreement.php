<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Auth;

class Agreement extends Model
{
    protected $fillable = ['user_id', 'terms_and_conditions'];
    protected $hidden = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createAgreement($user, $terms)
    {
        if ($terms != "" && $terms != null)
            $agreement = $this::create([
                'user_id' => $user,
                'terms_and_conditions' => $terms
            ]);
        return $agreement;
    }

    public function updateAgreement($terms, $id)
    {
        $agreement = $this::find($id);
        $agreement->terms_and_conditions = $terms;
        $agreement->save();
    }

    public function destroyAgreement($id)
    {
        $this::find($id)->delete();
    }
}