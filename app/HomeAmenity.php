<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Property;
use App\Amenity;

class HomeAmenity extends Model
{
    /**
     * Home Amenity is required for HomeOwners
     * Home Owners will add amenities to their property(home)
     * This model is a relationship between properties and amenity
     */

    protected $fillable = ['amenity_id', 'property_id'];
    protected $hidden = ['property_id'];

    public function properties()
    {
        return $this->hasMany(Property::class, 'id', 'property_id');
    }

    public function amenities()
    {
        return $this->belongsTo(Amenity::class, 'amenity_id', 'id');
    }

    public function addSingleAmenity($amenity, $property)
    {
        $amenity = $this::create([
            'amenity_id' => $amenity,
            'property_id' => $property
        ]);
        return $amenity;
    }

    public function addAmenity($amenity, $property)
    {
        for ($i = 0; $i < count($amenity); $i++) {
            $this::create([
                'amenity_id' => $amenity[$i],
                'property_id' => $property
            ]);
        }
    }
    public function updateAmenity($amenity, $room)
    {
        $this::where('property_id', $room)->delete();
        $this->addAmenity($amenity, $room);
    }
}