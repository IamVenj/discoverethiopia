<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    /**
     * RoomType is required to choose what type of room the client is going to stay in
     * RoomType is handled by the admin
     */
    protected $fillable = ['name', 'description', 'type'];
    public function createRoomType($name, $description, $type)
    {
        $roomType = $this::create([
            'name' => $name,
            'description' => $description,
            'type' => $type
        ]);
        return $roomType;
    }

    public function updateRoomType($name, $description, $type, $id)
    {
        $roomType = $this::find($id);
        $roomType->name = $name;
        $roomType->description = $description;
        $roomType->type = $type;
        $roomType->save();
    }

    public function destroyRoomType($id)
    {
        $this::find($id)->delete();
    }
}