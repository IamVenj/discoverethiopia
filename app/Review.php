<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;

class Review extends Model
{
    protected $fillable = ['reviewee_id', 'reviewer_id', 'review', 'rating', 'property_id', 'room_id', 'tour_id', 'car_id'];
    protected $hidden = ['reviewee_id', 'reviewer_id'];

    public function reviewer()
    {
        return $this->belongsTo(User::class, 'reviewer_id', 'id');
    }

    public function reviewee()
    {
        return $this->hasOne(User::class, 'id', 'reviewee_id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'id', 'property_id');
    }

    public function rooms()
    {
        return $this->hasMany(Room::class, 'id', 'room_id');
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'id', 'car_id');
    }

    public function tours()
    {
        return $this->hasMany(Tour::class, 'id', 'property_id');
    }

    /**
     * to calculate rating the assumption is
     * _ multiply weight with its raters
     * _ $main_scenario = count_of_people_who_rate * the_weight
     * _ then divide the rating with the total count of people who rated on that particular place
     * _ $main_scenario / count_of_raters
     * @param int $reviewee_id
     * @return int
     */

    public function calculateRating($reviewee_id)
    {
        $count_of_ppl_who_rated_5 = $this::where('reviewee_id', $reviewee_id)->where('rating', 5)->count();
        $count_of_ppl_who_rated_4 = $this::where('reviewee_id', $reviewee_id)->where('rating', 4)->count();
        $count_of_ppl_who_rated_3 = $this::where('reviewee_id', $reviewee_id)->where('rating', 3)->count();
        $count_of_ppl_who_rated_2 = $this::where('reviewee_id', $reviewee_id)->where('rating', 2)->count();
        $count_of_ppl_who_rated_1 = $this::where('reviewee_id', $reviewee_id)->where('rating', 1)->count();
        $countOfRaters = $this::where('reviewee_id', $reviewee_id)->count();
        $final = (5 * $count_of_ppl_who_rated_5 + 4 * $count_of_ppl_who_rated_4 + 3
            * $count_of_ppl_who_rated_3 + 2 * $count_of_ppl_who_rated_2
            + 1 * $count_of_ppl_who_rated_1) / $countOfRaters;
        return round($final);
    }

    /**
     * This is only for rooms
     * @param int $reviewee_id
     * @param int $room_id
     * @return int
     */

    public function calculateRatingForRooms($reviewee_id, $room_id)
    {
        $count_of_ppl_who_rated_5 = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->where('rating', 5)->count();
        $count_of_ppl_who_rated_4 = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->where('rating', 4)->count();
        $count_of_ppl_who_rated_3 = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->where('rating', 3)->count();
        $count_of_ppl_who_rated_2 = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->where('rating', 2)->count();
        $count_of_ppl_who_rated_1 = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->where('rating', 1)->count();
        $countOfRaters = $this::where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->count();
        $final = (5 * $count_of_ppl_who_rated_5 + 4 * $count_of_ppl_who_rated_4 + 3
            * $count_of_ppl_who_rated_3 + 2 * $count_of_ppl_who_rated_2
            + 1 * $count_of_ppl_who_rated_1) / $countOfRaters;
        return round($final);
    }


    /**
     * @param int $reviewee_id
     * @return int
     */
    public function totalCountOfReview($reviewee_id)
    {
        return $this::where('reviewee_id', $reviewee_id)->count();
    }

    public function getMyReviews($room_id, $tour_id, $property_id, $car_id)
    {
        if (!is_null($room_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('room_id', $room_id)->first();
        } elseif (!is_null($tour_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('tour_id', $tour_id)->first();
        } elseif (!is_null($car_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('car_id', $car_id)->first();
        } elseif (!is_null($property_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('property_id', $property_id)->first();
        }
    }

    public function checkIfReviewWasPosted($reviewee_id, $room_id, $tour_id, $car_id, $property_id)
    {
        if (!is_null($room_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('reviewee_id', $reviewee_id)->where('room_id', $room_id)->count();
        } elseif (!is_null($tour_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('reviewee_id', $reviewee_id)->where('tour_id', $tour_id)->count();
        } elseif (!is_null($car_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('reviewee_id', $reviewee_id)->where('car_id', $car_id)->count();
        } elseif (!is_null($property_id)) {
            return $this::where('reviewer_id', Auth::user()->id)->where('reviewee_id', $reviewee_id)->where('property_id', $property_id)->count();
        }
    }

    public function createReview($user_id, $reviewee_id, $review, $rating, $tour, $car, $room, $property)
    {
        $review = $this::create([
            'reviewer_id' => $user_id,
            'reviewee_id' => $reviewee_id,
            'review' => $review,
            'rating' => $rating,
            'tour_id' => $tour,
            'car_id' => $car,
            'property_id' => $property,
            'room_id' => $room
        ]);
        return $review;
    }

    public function updateReview($id, $review, $rating)
    {
        $r = $this::find($id);
        $r->review = $review;
        $r->rating = $rating;
        $r->save();
    }

    public function destroyReview($id)
    {
        $this::find($id)->delete();
    }
}