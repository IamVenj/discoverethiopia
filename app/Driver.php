<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Car;
use App\Address;

class Driver extends Model
{
    protected $fillable = ['name', 'description'];

    public function cars()
    {
        return $this->hasMany(Car::class, 'driver_id', 'id');
    }

    public function createDriver($name, $description)
    {
        $driver = $this::create([
            'name' => $name,
            'description' => $description,
        ]);
        return $driver;
    }

    public function updateDriver($name, $description, $id)
    {
        $driver = $this::find($id);
        $driver->name = $name;
        $driver->description = $description;
        $driver->save();
    }

    public function destroyDriver($id)
    {
        $this::find($id)->delete();
    }
}