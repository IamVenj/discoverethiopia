<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HomeOwner;
use App\HomeAmenity;
use App\Review;
use App\RoomType;
use App\HomeBooking;

class Property extends Model
{
    /**
     * Property Model is made for HomeOwners | VacationHomes and more...
     */
    protected $fillable = [
        'name', 'description', 'price_per_night', 'home_owner_id', 'address', 'latitude', 'longitude', 'availability',
        'maximum_capacity', 'bedroom', 'bed_detail', 'square_feet', 'bathroom', 'basic_layout', 'property_type'
    ];

    public function propertyType()
    {
        return $this->belongsTo(RoomType::class, 'property_type', 'id');
    }

    public function homeOwner()
    {
        return $this->hasOne(HomeOwner::class, 'user_id', 'home_owner_id');
    }

    public function images()
    {
        return $this->hasMany(PropertyImage::class);
    }

    public function amenities()
    {
        return $this->hasMany(HomeAmenity::class, 'property_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(HomeBooking::class, 'property_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'property_id', 'id');
    }
    public function recoupDataById($id)
    {
        return $this::with(['homeOwner.user', 'propertyType'])->find($id);
    }

    public function setAvailability($id, $availability)
    {
        $property = $this::find($id);
        $property->availability = $availability;
        $property->save();
    }

    public function checkAvailability($id)
    {
        return $this::find($id)->availability;
    }

    public function createProperty(
        $name,
        $description,
        $price,
        $ownerId,
        $address,
        $lat,
        $lng,
        $availability,
        $max_capacity,
        $bedroom,
        $bed_detail,
        $square_feet,
        $bathroom,
        $basic_layout,
        $property_type
    ) {
        $property = $this::create([
            'name' => $name,
            'description' => $description,
            'price_per_night' => $price,
            'home_owner_id' => $ownerId,
            'address' => $address,
            'latitude' => $lat,
            'longitude' => $lng,
            'availability' => $availability,
            'maximum_capacity' => $max_capacity,
            'bedroom' => $bedroom,
            'bed_detail' => $bed_detail,
            'square_feet' => $square_feet,
            'bathroom' => $bathroom,
            'basic_layout' => $basic_layout,
            'property_type' => $property_type
        ]);
        return $property;
    }

    public function updateProperty(
        $id,
        $name,
        $description,
        $price,
        $ownerId,
        $availability,
        $max_capacity,
        $bedroom,
        $bed_detail,
        $square_feet,
        $bathroom,
        $basic_layout,
        $property_type
    ) {
        $property = $this::find($id);
        $property->name = $name;
        $property->description = $description;
        $property->price_per_night = $price;
        $property->home_owner_id = $ownerId;
        $property->availability = $availability;
        $property->maximum_capacity = $max_capacity;
        $property->bedroom = $bedroom;
        $property->bed_detail = $bed_detail;
        $property->square_feet = $square_feet;
        $property->bathroom = $bathroom;
        $property->basic_layout = $basic_layout;
        $property->property_type = $property_type;
        $property->save();
    }

    public function destroyProperty($id)
    {
        $this::find($id)->delete();
    }
}