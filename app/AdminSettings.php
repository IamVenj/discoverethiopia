<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminSettings extends Model
{
    protected $fillable = ['hotel_agreement', 'tour_agreement', 'car_agreement', 'customer_agreement', 'home_agreement'];
    public function updateSettings($hotel_agreement, $tour_agreement, $car_agreement, $customer_agreement, $home_agreement)
    {
        $agreement = $this::first();
        $agreement->hotel_agreement = $hotel_agreement;
        $agreement->tour_agreement = $tour_agreement;
        $agreement->car_agreement = $car_agreement;
        $agreement->customer_agreement = $customer_agreement;
        $agreement->home_agreement = $home_agreement;
        $agreement->save();
    }
}