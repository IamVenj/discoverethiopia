<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;
use App\Amenity;

class HotelAmenity extends Model
{
    /**
     * Hotel Amenity is required for Hotels
     * Hotel Owners will add amenities to their rooms
     * This model is a relationship between rooms and amenity
     */

    protected $fillable = ['amenity_id', 'room_id'];
    protected $hidden = ['room_id'];

    public function room()
    {
        return $this->hasMany(Room::class, 'id', 'room_id');
    }

    public function amenities()
    {
        return $this->belongsTo(Amenity::class, 'amenity_id', 'id');
    }


    public function addSingleAmenity($amenity, $room)
    {
        $amenity = $this::create([
            'amenity_id' => $amenity,
            'room_id' => $room
        ]);
        return $amenity;
    }

    public function addAmenity($amenity, $room)
    {
        for ($i = 0; $i < count($amenity); $i++) {
            $this::create([
                'amenity_id' => $amenity[$i],
                'room_id' => $room
            ]);
        }
    }

    public function updateAmenity($amenity, $room)
    {
        $this::where('room_id', $room)->delete();
        $this->addAmenity($amenity, $room);
    }
}