<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tour;
use Illuminate\Support\Facades\Auth;

class TourOwner extends Model
{
    protected $fillable = [
        'user_id', 'description', 'logo', 'company_name',
        'user_cancellation_time', 'user_reschedule_time',
        'user_cancellation_refund_amount', 'user_reschedule_refund_amount'
    ];

    protected $hidden = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    public function tourst()
    {
        return $this->hasMany(Tour::class, 'tour_owner_id', 'user_id');
    }

    public function agreement()
    {
        return $this->hasOne(Agreement::class, 'user_id', 'user_id');
    }

    public function createTourOwner(
        $userId,
        $description,
        $logo,
        $company,
        $user_cancellation_time,
        $user_reschedule_time,
        $user_cancellation_refund_amount,
        $user_reschedule_refund_amount
    ) {
        $tour = $this::create([
            'user_id' => $userId,
            'description' => $description,
            'logo' => $logo,
            'company_name' => $company,
            'user_cancellation_time' => $user_cancellation_time,
            'user_reschedule_time' => $user_reschedule_time,
            'user_cancellation_refund_amount' => $user_cancellation_refund_amount,
            'user_reschedule_refund_amount' => $user_reschedule_refund_amount
        ]);
        return $tour;
    }

    public function updateTourOwner(
        $description,
        $logo,
        $companyName,
        $userCancellationTime,
        $userRescheduleTime,
        $userCancellationRefundAmount,
        $userRescheduleRefundAmount
    ) {
        $tourOwner = $this::where('user_id', Auth::user()->id)->first();
        $tourOwner->description = $description;
        $tourOwner->company_name = $companyName;
        $tourOwner->logo = $logo;
        $tourOwner->user_cancellation_time = $userCancellationTime;
        $tourOwner->user_reschedule_time = $userRescheduleTime;
        $tourOwner->user_cancellation_refund_amount = $userCancellationRefundAmount;
        $tourOwner->user_reschedule_refund_amount = $userRescheduleRefundAmount;
        $tourOwner->save();
    }
}